package com.sailboard.iot.opm.web.controller.company;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.dao.entity.*;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.service.company.CompanyService;
import com.sailboard.iot.opm.service.equipment.EquipmentService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/")
public class PartnerInfoController {

    @Resource
    UserService userService;

    @Resource
    CompanyService companyService;

    @Resource
    EquipmentService equipmentService;

    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    @RequestMapping(value = "v1/api0/partnerInfo/list")
    @ResponseBody
    public DataGrid<PartnerInfo> partnerInfoList(Integer page, Integer rows, Company company) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        //角色判断。
        Company companyTemp = companyService.getCompanyByID(String.valueOf(this.getCurrent_user().getCompanyId()));
        if (companyTemp != null){
            if(companyTemp.getLeader().equals(this.getCurrent_user().getId())){
                company.setBelongCompany(this.getCurrent_user().getCompanyId());
            }else{
                company.setFounder(this.getCurrent_user().getId());
            }
        }else {
            return null;
        }
        company.setBelongCompany(Long.valueOf(getCurrent_user().getCompanyId()));
        PageInfo pageInfo = companyService.getPartnerInfoList(page, rows, company);
        return new DataGrid(pageInfo);
    }


    @RequestMapping(value = "v1/api0/partnerInfo/showStation")
    @ResponseBody
    public DataGrid<ChargingStation> stationList(Integer page, Integer rows, String partnerId) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        ChargingStation chargingStation = new ChargingStation();
        chargingStation.setPartenerId(Long.valueOf(partnerId));
        PageInfo pageInfo = equipmentService.getChargingStationList(page,rows,chargingStation);
        return new DataGrid(pageInfo);
    }

    @RequestMapping(value = "v1/api0/partnerInfo/showConsole")
    @ResponseBody
    public DataGrid<Console> consoleList(Integer page, Integer rows, String partnerId) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        Console console = new Console();
        console.setStationId(Long.valueOf(partnerId));
        PageInfo pageInfo = equipmentService.getConsoleListByPartnerId(page,rows,console);
        return new DataGrid(pageInfo);
    }

    @RequestMapping(value = "v1/api0/partnerInfo/showTerminal")
    @ResponseBody
    public DataGrid<Terminal> terminalList(Integer page, Integer rows, String partnerId) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        Terminal terminal = new Terminal();
        terminal.setConsoleId(Long.valueOf(partnerId));
        PageInfo pageInfo = equipmentService.getTerminalListByPartnerId(page,rows,terminal);
        return new DataGrid(pageInfo);
    }
}
