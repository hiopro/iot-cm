package com.sailboard.iot.opm.web.controller;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.dao.entity.Account;
import com.sailboard.iot.opm.dao.entity.Agent_Account;
import com.sailboard.iot.opm.dao.entity.Company;
import com.sailboard.iot.opm.dao.entity.User;
import com.sailboard.iot.opm.service.AccountService;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.service.company.CompanyService;
import com.sailboard.iot.opm.utils.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/")
public class AccountController {
    @Resource AccountService accountService;
    @Resource
    UserService userService;
    @Resource
    CompanyService companyService;

    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    @RequestMapping(value = "v1/api0/Account/getCurrentAccount")
    @ResponseBody
    public  JsonResult getCurrentAccount(){
        Account account = accountService.getAccountByUserId(this.getCurrent_user().getCompanyId());
        return  new JsonResult(ResultCode.SUCCESS_CODE, ResultCode.SUCCESS_MSG,account);
    }

    @RequestMapping(value = "v1/api0/agent_Account/list")
    @ResponseBody
    public DataGrid agent_AccountList(Integer page, Integer rows, Agent_Account agent_account){
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
//        User user = this.getCurrent_user();
//        Long companyID = user.getCompanyId();
//        Company company1 = companyService.getCompanyByID(String.valueOf(companyID));
//        if (company1!=null){
//            if(company1.getLeader().equals(user.getId())){
//                agent_account.setCompanyBelong(companyID);
//            }else{
//                agent_account.setCompanyFounder(user.getId());
//            }
//        }else {
//            return null;
//        }//权限控制则打开
        PageInfo pageInfo = accountService.getAgent_AccountList(page,rows,agent_account);
        return  new DataGrid(pageInfo);
    }
    @RequestMapping(value = "v1/api0/account/bantchDisable", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult batchDisable(String ids) {
        if(StringUtils.isEmpty(ids)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = accountService.batchDisableCompany(ids);
        return (isSuccess)?JsonResult.success() : new JsonResult(ResultCode.ERROR_CODE,"状态未改变");
    }
    //禁用账户
    @RequestMapping(value = "v1/api0/account/disable", method = RequestMethod.POST)
    @ResponseBody
    public  JsonResult disable(String accountID){
        if (StringUtils.isBlank(accountID)){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = accountService.updateAccountStatus(Long.valueOf(accountID),2);
        return (isSuccess) ? JsonResult.success() : JsonResult.error();
    }
    //启用账户
    @RequestMapping(value = "v1/api0/account/enable", method = RequestMethod.POST)
    @ResponseBody
    public  JsonResult enable(String accountID){
        if (StringUtils.isBlank(accountID)){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = accountService.updateAccountStatus(Long.valueOf(accountID),1);
        return (isSuccess) ? JsonResult.success() : JsonResult.error();
    }

    //查看账户详情
    @RequestMapping(value = "v1/api0/account/detail")
    @ResponseBody
    public  JsonResult accountDetail ( String companyID){
        if (StringUtils.isBlank(companyID)){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        Account account = accountService.getAccountByUserId(Long.valueOf(companyID));
        return  new JsonResult(ResultCode.SUCCESS_CODE,ResultCode.SUCCESS_MSG,account);
    }
}
