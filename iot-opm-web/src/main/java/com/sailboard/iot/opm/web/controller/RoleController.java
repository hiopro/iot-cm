
package com.sailboard.iot.opm.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.Constants;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.common.Tree;
import com.sailboard.iot.opm.dao.entity.*;
import com.sailboard.iot.opm.service.ResourceService;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.service.company.CompanyService;
import com.sailboard.iot.opm.service.impl.RoleService;
import com.sailboard.iot.opm.utils.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by xuejt on 2015/4/3.
 */
@Controller
@RequestMapping(value = "/")
public class RoleController {

    @javax.annotation.Resource
    UserService userService;
    @javax.annotation.Resource
    RoleService roleService;
    @javax.annotation.Resource
    ResourceService resourceService;
    @javax.annotation.Resource
    CompanyService companyService;

    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    @RequestMapping(value = "v1/api0/role/list")
    @ResponseBody
    public DataGrid<User> userList(Integer page, Integer rows, Role role) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        User current_user = getCurrent_user();
        Company current_company = companyService.getCompanyById(getCurrent_user().getCompanyId());
        String type =  String.valueOf(current_company.getType());//根据公司类型查询对应的角色。
        PageInfo pageInfo = roleService.getRoleList(page, rows, type );
        return new DataGrid(pageInfo);
    }
    @RequestMapping(value = "v1/api0/all/role/list")
    @ResponseBody
    public DataGrid<User> roleListAll(Integer page, Integer rows, Role role) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = roleService.getRoleListAll(page, rows,role);
        return new DataGrid(pageInfo);
    }

    @RequestMapping(value = "v1/api0/role/add")
    @ResponseBody
    public JsonResult add(Role role) {
        if(role == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(role.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "角色名称不能为空");
        }
        if(StringUtils.isBlank(role.getType())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "角色类型不能为空");
        }
        if(role.getIsManager() .equals("0")){
            if(roleService.getRoleByIsMgrAndType("0",role.getType()) != null){
                return new JsonResult(ResultCode.PARAM_ERROR_CODE, "每个类型的公司只能有一个管理员角色");
            }
        }

        boolean isSuccess = roleService.addRole(role);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/role/edit")
    @ResponseBody
    public JsonResult edit(Role role) {
        if(role == null || role.getId() == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(role.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "角色名称不能为空");
        }
        if(StringUtils.isBlank(role.getType())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "角色类型不能为空");
        }
        if(role.getIsManager() .equals("0")){
            if(roleService.getOtherRoleByIsMgrAndType("0",role.getType(),role.getId()) != null){
                return new JsonResult(ResultCode.PARAM_ERROR_CODE, "每个类型的公司只能有一个管理员角色");
            }
        }

        boolean isSuccess = roleService.updateRole(role);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/role/delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult delete(String ids) {
        return roleService.batchDeleteRole(ids);
    }

    @RequestMapping(value = "v1/api0/role/resource")
    @ResponseBody
    public List<Tree> resourceList(Long roleId) {
        List<RoleResource> roleResourceList = roleService.getResourcesByRoleId(roleId);
        List<Resource> resourceList = resourceService.getResourceList();
        List<Tree> treeList = wrapTree(resourceList, 0);
        treeNoteChecked(treeList, roleResourceList);
        return treeList;
    }

    private List<Tree> wrapTree(List<Resource> resourceList, long pid) {
        List<Tree> treeList = new ArrayList<>();
        if(resourceList == null || resourceList.size() == 0) {
            return treeList;
        }
        for (Resource resource : resourceList) {
            if (pid == resource.getPid()) {
                Tree tree = new Tree(resource.getId(), resource.getPid(), resource.getName(), resource.getIcon(), resource.getSort(), resource.getUrl());
                List<Tree> children = wrapTree(resourceList, resource.getId());
                Collections.sort(children, (arg0, arg1) -> (int)(arg0.getSort() - arg1.getSort()));
                tree.setChildren(children);
                treeList.add(tree);
            }
        }
        Collections.sort(treeList, (arg0, arg1) -> (int)(arg0.getSort() - arg1.getSort()));
        return treeList;
    }

    private void treeNoteChecked(List<Tree> treeList, List<RoleResource> roleResourceList) {
        if(roleResourceList == null && roleResourceList.size() == 0) {
            return;
        }
        for(Tree tree : treeList) {
            List<Tree> children = tree.getChildren();
            if(children == null || children.size() == 0) {
                for(RoleResource roleResource : roleResourceList) {
                    if(tree.getId() == roleResource.getResourceId()) {
                        tree.setChecked(true);
                        break;
                    }
                }
            } else {
                treeNoteChecked(children, roleResourceList);
            }
        }
    }

    @RequestMapping(value = "v1/api0/role/bindResource", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult bindResource(Long roleId, String resourceIds) {
        if(roleId == null || roleId <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        return roleService.bindResource(roleId, resourceIds);
    }

    @RequestMapping(value = "v1/api0/role/enable", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enable(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = roleService.updateRoleStatus(id, Constants.ROLE_STATUS_ENABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/role/disable", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disable(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = roleService.updateRoleStatus(id, Constants.ROLE_STATUS_DISABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/role/currentList")
    @ResponseBody
    public DataGrid<User> currentRoleList(Integer page, Integer rows, Role role) {
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        List<Role> list =new ArrayList<Role>();
        Long userId=null;
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            User user = userService.getByUsername(username);
            userId=user.getId();
        }
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = roleService.getCurrRoleList(page, rows,userId);
        return new DataGrid(pageInfo);
    }
}
