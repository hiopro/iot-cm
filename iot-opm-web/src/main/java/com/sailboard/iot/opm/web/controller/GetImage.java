package com.sailboard.iot.opm.web.controller;

import com.sailboard.iot.opm.common.SystemCodeConstants;
import com.sailboard.iot.opm.service.qrcode.OssUtil;
import com.sailboard.iot.opm.utils.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class GetImage extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response)
			throws ServletException, IOException {
		String businessType=req.getParameter("bt");//业务类型
		String fileName= req.getParameter("fn");
		String subdirectory=req.getParameter("sd");//子目录
		if(StringUtils.isEmpty(subdirectory)){
			subdirectory="";
		}
		fileName=new String(fileName.getBytes("ISO-8859-1"),"UTF-8");

		if(fileName != null){
			String miniType ="";
			if(fileName.lastIndexOf(".")!=-1){
				String[] spit=fileName.split("\\.");
				miniType = spit[spit.length-1].toUpperCase();
			}
			boolean okflag=false;
			//支持的文件类型
			String[] accept={"jpg","jpeg","gif","png","jpe","wmv","flv","mp4"};
			for(String s:accept){
				if(miniType.contains(s.toUpperCase())){
					okflag=true;
					break;
				}
			}

			if(!okflag){
				response.getOutputStream().write("File not found!Please check URL!".getBytes());
				return;
			}
			//SystemParamConfig.systemParamMap.get(businessType);
            String filePath= SystemCodeConstants.STATIC_UPLOAD_URL+File.separator+businessType;//Oss路径
			//String filePath= SystemCodeConstants.STATIC_UPLOAD_URL+"/"+businessType;//本地路径
			if(StringUtils.isBlank(filePath)){
				response.getOutputStream().write("File not found!Please check URL!".getBytes());
				return;
			}else{
				filePath=filePath+File.separator;//服务器用
				//filePath=filePath+"/";//本地测试用
			}

			File file = null;
			ServletOutputStream out = response.getOutputStream();
			//FileInputStream in = null;
			InputStream in = null;
			try {
//				file = new File(filePath+subdirectory+fileName);
//				if(file.exists()){
//
//
//				in = new FileInputStream(file);
//				ByteArrayOutputStream baos = new ByteArrayOutputStream();
//				byte b[] = new byte[1024];
//				while (true) {
//					int bytes = in.read(b);
//					if (bytes == -1) {
//						break;
//					}
//					baos.write(b, 0, bytes);
//				}
//				in.close();
//				b = baos.toByteArray();
//
//				response.setContentType(getMiniType(miniType));
//				response.setContentLength(b.length);
//				out.write(b, 0, b.length);
//				out.flush();
//				out.close();
//				}
//				else{
//					response.getOutputStream().write("File not found!Please check URL!".getBytes());
//				}
				/**
				 * 切换阿里云oss存储方式
				 */
				filePath=filePath+subdirectory+fileName;//OSS用
				in= OssUtil.getFileFromOss(filePath);
				if(in!=null){
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte b[] = new byte[1024];
					while (true) {
						int bytes = in.read(b);
						if (bytes == -1) {
							break;
						}
						baos.write(b, 0, bytes);
					}
					in.close();
					b = baos.toByteArray();

					response.setContentType(getMiniType(miniType));
					response.setContentLength(b.length);
					out.write(b, 0, b.length);
					out.flush();
					out.close();
				}else{
					response.getOutputStream().write("File not found!Please check URL!".getBytes());
				}
			} catch (Exception e) {
				e.printStackTrace();
				response.getOutputStream().write("File not found!Please check URL!".getBytes());
			}
		}else{
			response.getOutputStream().write("File not found!Please check URL!".getBytes());
		}
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(req,response);
	}

	private String getMiniType(String extName) {
		/* response.setCharacterEncoding("GBK");
         BufferedOutputStream bos = new BufferedOutputStream(
                 response.getOutputStream());
         byte[] input = new byte[1024];
         boolean eof = false;
         while (!eof) {
             int length = bis.read(input);
             if (length == -1) {
                 eof = true;
             } else {
                 bos.write(input, 0, length);
             }
         }
         bos.flush();
         bis.close();
         bos.close();
		*/

		if (extName.equals("tif")) {
			return "image/tiff";
		} else if (extName.equals("tiff")) {
			return "image/tiff";
		} else if (extName.equals("bmp")) {
			return "image/bmp";
		} else if (extName.equals("gif")) {
			return "image/gif";
		} else if (extName.equals("jpg")) {
			return "image/jpeg";
		} else if (extName.equals("jpe")) {
			return "image/jpeg";
		} else if (extName.equals("jpeg")) {
			return "image/jpeg";
		} else if (extName.equals("doc")) {
			return "application/msword";
		} else if (extName.equals("dot")) {
			return "application/msword";
		} else if (extName.equals("rtf")) {
			return "application/msword";
		} else if (extName.equals("xls")) {
			return "application/vnd.ms-excel";
		} else if (extName.equals("ppt")) {
			return "application/vnd.ms-powerpoint";
		} else if (extName.equals("vsd")) {
			return "application/vnd.visio";
		} else if (extName.equals("wp")) {
			return "application/wordperfect5.1";
		} else if (extName.equals("txt")) {
			return "text/plain";
		} else if (extName.equals("html")) {
			return "text/html";
		} else if (extName.equals("htm")) {
			return "text/html";
		} else if (extName.equals("txt")) {
			return "text/plain";
		} else if (extName.equals("java")) {
			return "text/plain";
		} else if (extName.equals("body")) {
			return "text/html";
		} else if (extName.equals("rtx")) {
			return "text/richtext";
		} else if (extName.equals("tsv")) {
			return "text/tab-separated-values";
		} else if (extName.equals("etx")) {
			return "text/x-setext";
		} else if (extName.equals("ps")) {
			return "application/x-postscript";
		} else if (extName.equals("class")) {
			return "application/java";
		} else if (extName.equals("csh")) {
			return "application/x-csh";
		} else if (extName.equals("sh")) {
			return "application/x-sh";
		} else if (extName.equals("tcl")) {
			return "application/x-tcl";
		} else if (extName.equals("tex")) {
			return "application/x-tex";
		} else if (extName.equals("texinfo")) {
			return "application/x-texinfo";
		} else if (extName.equals("texi")) {
			return "application/x-texinfo";
		} else if (extName.equals("t")) {
			return "application/x-troff";
		} else if (extName.equals("tr")) {
			return "application/x-troff";
		} else if (extName.equals("roff")) {
			return "application/x-troff";
		} else if (extName.equals("man")) {
			return "application/x-troff-man";
		} else if (extName.equals("me")) {
			return "application/x-troff-me";
		} else if (extName.equals("ms")) {
			return "application/x-wais-source";
		} else if (extName.equals("src")) {
			return "application/x-wais-source";
		} else if (extName.equals("zip")) {
			return "application/zip";
		} else if (extName.equals("bcpio")) {
			return "application/x-bcpio";
		} else if (extName.equals("cpio")) {
			return "application/x-cpio";
		} else if (extName.equals("gtar")) {
			return "application/x-gtar";
		} else if (extName.equals("shar")) {
			return "application/x-shar";
		} else if (extName.equals("sv4cpio")) {
			return "application/x-sv4cpio";
		} else if (extName.equals("sv4crc")) {
			return "application/x-sv4crc";
		} else if (extName.equals("tar")) {
			return "application/x-tar";
		} else if (extName.equals("ustar")) {
			return "application/x-ustar";
		} else if (extName.equals("dvi")) {
			return "application/x-dvi";
		} else if (extName.equals("hdf")) {
			return "application/x-hdf";
		} else if (extName.equals("latex")) {
			return "application/x-latex";
		} else if (extName.equals("bin")) {
			return "application/octet-stream";
		} else if (extName.equals("oda")) {
			return "application/oda";
		} else if (extName.equals("pdf")) {
			return "application/pdf";
		} else if (extName.equals("ps")) {
			return "application/postscript";
		} else if (extName.equals("eps")) {
			return "application/postscript";
		} else if (extName.equals("ai")) {
			return "application/postscript";
		} else if (extName.equals("rtf")) {
			return "application/rtf";
		} else if (extName.equals("nc")) {
			return "application/x-netcdf";
		} else if (extName.equals("cdf")) {
			return "application/x-netcdf";
		} else if (extName.equals("cer")) {
			return "application/x-x509-ca-cert";
		} else if (extName.equals("exe")) {
			return "application/octet-stream";
		} else if (extName.equals("gz")) {
			return "application/x-gzip";
		} else if (extName.equals("Z")) {
			return "application/x-compress";
		} else if (extName.equals("z")) {
			return "application/x-compress";
		} else if (extName.equals("hqx")) {
			return "application/mac-binhex40";
		} else if (extName.equals("mif")) {
			return "application/x-mif";
		} else if (extName.equals("ief")) {
			return "image/ief";
		} else if (extName.equals("ras")) {
			return "image/x-cmu-raster";
		} else if (extName.equals("pnm")) {
			return "image/x-portable-anymap";
		} else if (extName.equals("pbm")) {
			return "image/x-portable-bitmap";
		} else if (extName.equals("pgm")) {
			return "image/x-portable-graymap";
		} else if (extName.equals("ppm")) {
			return "image/x-portable-pixmap";
		} else if (extName.equals("rgb")) {
			return "image/x-rgb";
		} else if (extName.equals("xbm")) {
			return "image/x-xbitmap";
		} else if (extName.equals("xpm")) {
			return "image/x-xpixmap";
		} else if (extName.equals("xwd")) {
			return "image/x-xwindowdump";
		} else if (extName.equals("au")) {
			return "audio/basic";
		} else if (extName.equals("snd")) {
			return "audio/basic";
		} else if (extName.equals("aif")) {
			return "audio/x-aiff";
		} else if (extName.equals("aiff")) {
			return "audio/x-aiff";
		} else if (extName.equals("aifc")) {
			return "audio/x-aiff";
		} else if (extName.equals("wav")) {
			return "audio/x-wav";
		} else if (extName.equals("mpeg")) {
			return "video/mpeg";
		} else if (extName.equals("mpg")) {
			return "video/mpeg";
		} else if (extName.equals("mpe")) {
			return "video/mpeg";
		} else if (extName.equals("qt")) {
			return "video/quicktime";
		} else if (extName.equals("mov")) {
			return "video/quicktime";
		} else if (extName.equals("avi")) {
			return "video/x-msvideo";
		} else if (extName.equals("movie")) {
			return "video/x-sgi-movie";
		} else if (extName.equals("avx")) {
			return "video/x-rad-screenplay";
		} else if (extName.equals("wrl")) {
			return "x-world/x-vrml";
		} else if (extName.equals("mpv2")) {
			return "video/mpeg2";
		} else if (extName.equals("xml")) {
			return "text/xml";
		}else if (extName.equals("syd")) {
			return "text/xml";
		}else {
			return "application/octet-stream";
		}
	}
}
