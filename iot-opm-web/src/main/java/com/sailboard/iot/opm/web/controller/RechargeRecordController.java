package com.sailboard.iot.opm.web.controller;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.dao.entity.RechargeRecord;
import com.sailboard.iot.opm.service.RechargeRecordService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/")
public class RechargeRecordController {
    @Resource
    RechargeRecordService rechargeRecordService;
    @RequestMapping(value = "v1/api0/recharge_Record/list")
    @ResponseBody
    public DataGrid listRecharge_Record(Integer page, Integer rows, RechargeRecord rechargeRecord){
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = rechargeRecordService.getRecharge_RecordList(page,rows,rechargeRecord);
        return  new DataGrid(pageInfo);
    }
}
