package com.sailboard.iot.opm.web.controller.equipment;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.Constants;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.dao.entity.*;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.service.equipment.EquipmentConfigService;
import com.sailboard.iot.opm.utils.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.web.controller.equipment
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/27 21:33
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/27 21:33
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
@RestController
@RequestMapping("/v1/api0/")
public class EquipmentConfigController {
    @Resource
    EquipmentConfigService equipmentConfigService;
    @Resource
    UserService userService;

    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    //获取充值卡列表
    @RequestMapping(value = "equipmentConfig/rechargeCardList")
    @ResponseBody
    public DataGrid<RechargeCard> rechargeCardList(Integer page, Integer rows, RechargeCard rechargeCard) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        rechargeCard.setCreaterId(this.getCurrent_user().getId());
        PageInfo pageInfo = equipmentConfigService.getRechargeCardList(page, rows, rechargeCard);
        return new DataGrid(pageInfo);
    }

    //增加充值卡
    @RequestMapping(value = "equipmentConfig/addRechargeCard")
    @ResponseBody
    public JsonResult addRechargeCard(RechargeCard rechargeCard) {
        if(rechargeCard == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(rechargeCard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "充值卡名称不能为空");
        }
        rechargeCard.setCreaterId(this.getCurrent_user().getId());
        rechargeCard.setCreater(this.getCurrent_user().getRealName());
        boolean isSuccess = equipmentConfigService.addRechargeCard(rechargeCard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //编辑充值卡
    @RequestMapping(value = "equipmentConfig/editRechargeCard")
    @ResponseBody
    public JsonResult editRechargeCard(RechargeCard rechargeCard) {
        if(rechargeCard == null || rechargeCard.getId() == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(rechargeCard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "充值卡名称不能为空");
        }
        boolean isSuccess = equipmentConfigService.updateRechargeCard(rechargeCard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //禁用充值卡
    @RequestMapping(value = "equipmentConfig/disableRechargeCard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disableRechargeCard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateRechargeCardStatus(id, 3);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //启用充值卡
    @RequestMapping(value = "equipmentConfig/enableRechargeCard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enableRechargeCard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateRechargeCardStatus(id, 1);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //发布充值卡
    @RequestMapping(value = "equipmentConfig/releaseRechargeCard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult releaseRechargeCard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateRechargeCardStatus(id, 2);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }
    //删除充值卡
    @RequestMapping(value = "equipmentConfig/deleteRechargeCard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult deleteRechargeCard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.deleteRechargeCard(id);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //批量删除充值卡
    @RequestMapping(value = "equipmentConfig/batchDelRechargeCard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult batchDelRechargeCard(String ids) {
        if(ids == null || ids.equals("")) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.batchDelRechargeCard(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //获取计费标准列表
    @RequestMapping(value = "equipmentConfig/chargeStandardList")
    @ResponseBody
    public DataGrid<ChargeStandard> chargeStandardList(Integer page, Integer rows, ChargeStandard chargeStandard) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        chargeStandard.setCreaterId(userService.getByUsername(this.getCurrent_user().getUsername()).getCompanyId());
        PageInfo pageInfo = equipmentConfigService.getChargeStandardList(page, rows, chargeStandard);
        return new DataGrid(pageInfo);
    }

    //增加计费标准
    @RequestMapping(value = "equipmentConfig/addChargeStandard")
    @ResponseBody
    public JsonResult addChargeStandard(ChargeStandard chargeStandard) {
        if(chargeStandard == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(chargeStandard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "计费标准名称不能为空");
        }
        chargeStandard.setCreaterId(userService.getByUsername(this.getCurrent_user().getUsername()).getCompanyId());
        chargeStandard.setCreater(userService.getByUsername(this.getCurrent_user().getUsername()).getCompanyName());
        boolean isSuccess = equipmentConfigService.addChargeStandard(chargeStandard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //编辑计费标准
    @RequestMapping(value = "equipmentConfig/editChargeStandard")
    @ResponseBody
    public JsonResult editChargeStandard(ChargeStandard chargeStandard) {
        if(chargeStandard == null || chargeStandard.getId() == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(chargeStandard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "计费标准名称不能为空");
        }
        boolean isSuccess = equipmentConfigService.updateChargeStandard(chargeStandard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //禁用计费标准
    @RequestMapping(value = "equipmentConfig/disableChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disableChargeStandard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateChargeStandardStatus(id, Constants.ROLE_STATUS_DISABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //启用计费标准
    @RequestMapping(value = "equipmentConfig/enableChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enableChargeStandard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateChargeStandardStatus(id, Constants.ROLE_STATUS_ENABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }
    //删除计费标准
    @RequestMapping(value = "equipmentConfig/deleteChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult deleteChargeStandard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.deleteChargeStandard(id);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //批量删除计费标准
    @RequestMapping(value = "equipmentConfig/batchDelChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult batchDelChargeStandard(String ids) {
        if(ids == null || ids.equals("")) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.batchDelChargeStandard(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //获取某个中控设备的计费标准列表
    @RequestMapping(value = "equipmentConfig/showConsoleChargeStandard")
    @ResponseBody
    public DataGrid<ConsoleChargeStandard> consoleChargeStandardDataGrid(Integer page, Integer rows, ConsoleChargeStandard consoleChargeStandard) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = equipmentConfigService.getConsoleChargeStandardList(page, rows, consoleChargeStandard);
        return new DataGrid(pageInfo);
    }

    //禁用中控计费标准
    @RequestMapping(value = "equipmentConfig/disableConsoleChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disableConsoleChargeStandard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateConsoleChargeStandardStatus(id, Constants.ROLE_STATUS_DISABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //启用中控计费标准
    @RequestMapping(value = "equipmentConfig/enableConsoleChargeStandard", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enableConsoleChargeStandard(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateConsoleChargeStandardStatus(id, Constants.ROLE_STATUS_ENABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //获取充电方式列表
    @RequestMapping(value = "equipmentConfig/chargeMethodList")
    @ResponseBody
    public DataGrid<ChargeMethod> chargeMethodList(Integer page, Integer rows, ChargeMethod chargeMethod) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = equipmentConfigService.getChargeMethodList(page, rows, chargeMethod);
        return new DataGrid(pageInfo);
    }

    //增加充电方式
    @RequestMapping(value = "equipmentConfig/addChargeMethod")
    @ResponseBody
    public JsonResult addChargeMethod(ChargeMethod chargeMethod) {
        if(chargeMethod == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(chargeMethod.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "充电方式名称不能为空");
        }

        boolean isSuccess = equipmentConfigService.addChargeMethod(chargeMethod);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //编辑充电方式
    @RequestMapping(value = "equipmentConfig/editChargeMethod")
    @ResponseBody
    public JsonResult editChargeMethod(ChargeMethod chargeMethod) {
        if(chargeMethod == null || chargeMethod.getId() == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(chargeMethod.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "充电方式名称不能为空");
        }
        boolean isSuccess = equipmentConfigService.updateChargeMethod(chargeMethod);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //禁用充电方式
    @RequestMapping(value = "equipmentConfig/disableChargeMethod", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disableChargeMethod(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateChargeMethodStatus(id, Constants.ROLE_STATUS_DISABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //启用充电方式
    @RequestMapping(value = "equipmentConfig/enableChargeMethod", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enableChargeMethod(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.updateChargeMethodStatus(id, Constants.ROLE_STATUS_ENABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }
    //删除充电方式
    @RequestMapping(value = "equipmentConfig/deleteChargeMethod", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult deleteChargeMethod(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.deleteChargeMethod(id);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //批量删除充电方式
    @RequestMapping(value = "equipmentConfig/batchDelChargeMethod", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult batchDelChargeMethod(String ids) {
        if(ids == null || ids.equals("")) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.batchDelChargeMethod(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }





    //增加中控的计费标准
    @RequestMapping(value = "equipmentConfig/consoleChargeStandard/add")
    @ResponseBody
    public JsonResult addConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard) {
        if(consoleChargeStandard == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(consoleChargeStandard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "计费标准名称不能为空");
        }
        consoleChargeStandard.setCreaterId(this.getCurrent_user().getId());
        consoleChargeStandard.setCreater(this.getCurrent_user().getRealName());
        boolean isSuccess = equipmentConfigService.addConsoleChargeStandard(consoleChargeStandard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //编辑计费标准
    @RequestMapping(value = "equipmentConfig/consoleChargeStandard/edit")
    @ResponseBody
    public JsonResult editConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard) {
        if(consoleChargeStandard == null || consoleChargeStandard.getId() == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(consoleChargeStandard.getName())) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "计费标准名称不能为空");
        }
        boolean isSuccess = equipmentConfigService.updateConsoleChargeStandard(consoleChargeStandard);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //批量删除计费标准
    @RequestMapping(value = "equipmentConfig/consoleChargeStandard/delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult batchDelConsoleChargeStandard(String ids) {
        if(ids == null || ids.equals("")) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = equipmentConfigService.batchDelConsoleChargeStandard(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

}
