package com.sailboard.iot.opm.web.controller;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.dao.entity.DepartmentUser;
import com.sailboard.iot.opm.dao.entity.User;
import com.sailboard.iot.opm.service.DepartmentService;
import com.sailboard.iot.opm.service.DepartmentUserService;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.utils.StringUtils;
import com.sailboard.iot.opm.utils.Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/")
public class DepartmentUserController {

    @Resource
    UserService userService;
    @Resource
    DepartmentUserService departmentUserService;
    @Resource
    DepartmentService departmentService;

    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    @RequestMapping(value = "v1/api0/departmentUser/list")
    @ResponseBody
    public DataGrid<User> userList(Integer page, Integer rows, DepartmentUser departmentUser) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = departmentUserService.getDepartmentUserList(page, rows, departmentUser);
        return new DataGrid(pageInfo);
    }
    @RequestMapping(value = "v1/api0/departmentUser/delete")
    @ResponseBody
    public JsonResult delete(String ids){
        if(StringUtils.isEmpty(ids)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = departmentUserService.batchDelete(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }


    //平台公司人员管理添加
    @RequestMapping(value = "v1/api0/departmentUser/add")
    @ResponseBody
    public JsonResult departmentUserAdd(User user) {
        if(user == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(!Utils.checkMobile(user.getMobile())){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "手机号格式错误");
        }
        user.setMobile(user.getMobile().trim());
        if(userService.getUserByMobile(user.getMobile()) != null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "该手机号已存在，请重新输入");
        }
        user.setUsername(user.getMobile());//手机号作为登录名
        user.setPassword("111111");
        String passowrd = user.getPassword().trim();
        String salt = Utils.generateSalt();
        user.setSalt(salt);
        user.setPassword(Utils.encryptPassword(salt, passowrd));
        User current_user=getCurrent_user();
        if(current_user!=null) {
            user.setCompanyId(current_user.getCompanyId());
            user.setCompanyName(current_user.getCompanyName());
        }
        user.setDepartmentName(departmentService.getDepartmentById(Long.valueOf(user.getDepartmentID())).getName());//通过部门id获取到部门名称一并存储。
        boolean isSuccess = userService.addUser(user);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    //平台公司人员管理编辑
    @RequestMapping(value = "v1/api0/departmentUser/edit")
    @ResponseBody
    public JsonResult departmentUserEdit(User user) {
        if(user == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(!Utils.checkMobile(user.getMobile())){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "手机号格式错误");
        }
        user.setMobile(user.getMobile().trim());
        if(userService.getOtherUserByMobile(user.getMobile(),user.getId()) != null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "该手机号已存在，请重新输入");
        }
        user.setUsername(user.getMobile());//手机号作为登录名
        user.setPassword("111111");
        String passowrd = user.getPassword().trim();
        String salt = Utils.generateSalt();
        user.setSalt(salt);
        user.setPassword(Utils.encryptPassword(salt, passowrd));

        user.setUsername(user.getUsername().trim());
        user.setDepartmentName(departmentService.getDepartmentById(Long.valueOf(user.getDepartmentID())).getName());//通过部门id获取到部门名称一并存储。
        boolean isSuccess = userService.updateUser(user);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

}
