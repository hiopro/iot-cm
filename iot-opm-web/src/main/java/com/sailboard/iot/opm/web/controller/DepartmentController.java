
package com.sailboard.iot.opm.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.Constants;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.dao.entity.Department;
import com.sailboard.iot.opm.dao.entity.Role;
import com.sailboard.iot.opm.dao.entity.User;
import com.sailboard.iot.opm.service.DepartmentService;
import com.sailboard.iot.opm.service.UserService;
import com.sailboard.iot.opm.service.impl.RoleService;
import com.sailboard.iot.opm.utils.StringUtils;
import com.sailboard.iot.opm.utils.Utils;
import net.sf.json.JSONArray;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by xuejt on 2015/4/3.
 */
@Controller
@RequestMapping(value = "/")
public class DepartmentController {

    @Resource
    UserService userService;
    @Resource
    RoleService roleService;
    @Resource
    DepartmentService departmentService;
    private User getCurrent_user(){
        User current_user=new User();
        Subject currentUser = SecurityUtils.getSubject();//获取当前用户
        if(currentUser != null && currentUser.getPrincipals() != null) {
            String username = currentUser.getPrincipals().toString();
            current_user = userService.getByUsername(username);
        }
        return current_user;
    }

    @RequestMapping(value = "v1/api0/department/list")
    @ResponseBody
    public DataGrid<Department> departmentList(Integer page,Integer rows,Department department){
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        User currentUser = getCurrent_user();
        if(getCurrent_user() != null){
            department.setCompanyId(String.valueOf(currentUser.getCompanyId()));
        }
        PageInfo pageInfo = departmentService.getDepartmentList(page,rows,department);
        return new DataGrid(pageInfo);
    }

    @RequestMapping(value = "v1/api0/departmentSelect/list")
    @ResponseBody
    public JSONArray departmentSelectList(Integer page,Integer rows,Department department){
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        User currentUser = getCurrent_user();
        if(getCurrent_user() != null){
            department.setCompanyId(String.valueOf(currentUser.getCompanyId()));
        }
       JSONArray jsonArray=new JSONArray() ;
        List<Department> list= new ArrayList<Department>();
        try{
            list = departmentService.getDepartmentList(page,rows,department).getList();
            jsonArray = net.sf.json.JSONArray.fromObject(list);
        }catch(Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

   @RequestMapping(value = "v1/api0/department/add")
    @ResponseBody
    public JsonResult add(Department department){
        if(department == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(department.getName())){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE,"部门名称不能为空");
        }
        if(StringUtils.isBlank(String.valueOf(department.getSequenceNumber()))){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE,"部门序号不能为空");
        }
       if(departmentService.getDepartmentByName(department.getName(),this.getCurrent_user().getCompanyId()) != null){
           return new JsonResult(ResultCode.PARAM_ERROR_CODE, "该部门已存在");
       }
       department.setCompanyId(String.valueOf(getCurrent_user().getCompanyId()));
        boolean isSuccess = departmentService.addDepartment(department);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/department/edit")
    @ResponseBody
    public JsonResult edit(Department department){
        if(department == null) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        if(StringUtils.isBlank(department.getName())){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE,"部门名称不能为空");
        }
        if(StringUtils.isBlank(String.valueOf(department.getSequenceNumber()))){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE,"部门序号不能为空");
        }
        if(departmentService.getOtherDepartmentByName(department.getName(),department.getId(),this.getCurrent_user().getCompanyId()) != null){
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "该部门已存在");
        }
        department.setCompanyId(String.valueOf(getCurrent_user().getCompanyId()));
        boolean isSuccess = departmentService.updateDepartment(department);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/department/delete")
    @ResponseBody
    public JsonResult delete(String ids){
        if(StringUtils.isEmpty(ids)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = departmentService.batchDeleteDepartment(ids);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/department/enable", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult enable(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess = departmentService.updateDepartmentStatus(id, Constants.USER_STATUS_ENABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/department/disable", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult disable(Long id) {
        if(id == null || id <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        boolean isSuccess =departmentService.updateDepartmentStatus(id, Constants.USER_STATUS_DISABLE);
        return isSuccess ? JsonResult.success() : JsonResult.error();
    }

    @RequestMapping(value = "v1/api0/department/user", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult departmentUser(Long userId) {
        if(userId == null || userId <= 0) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        List<Department> list = departmentService.getUserDepartment(userId);
        return new JsonResult<>(ResultCode.SUCCESS_CODE, ResultCode.SUCCESS_MSG, list);
    }
}

