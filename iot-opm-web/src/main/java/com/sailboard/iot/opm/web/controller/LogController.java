package com.sailboard.iot.opm.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.LoggerVM;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liyl
 * @date: 2017/12/13 上午9:29
 * @since 1.0.0
 */
@RestController
@RequestMapping("/v1/api0/")
public class LogController {

    @RequestMapping(value = "logs")
    public DataGrid<LoggerVM> getList() {
        LoggerContext context = (LoggerContext)LoggerFactory.getILoggerFactory();
        List<LoggerVM> loggerVMS = context.getLoggerList()
            .stream()
            .map(LoggerVM::new)
            .collect(Collectors.toList());
        return new DataGrid<LoggerVM>(loggerVMS.size(),loggerVMS);
    }

    @RequestMapping(value = "logs/set",method = RequestMethod.POST)
    @ResponseBody
    public JsonResult changeLevel(LoggerVM jsonLogger) {
        LoggerContext context = (LoggerContext)LoggerFactory.getILoggerFactory();
        context.getLogger(jsonLogger.getName()).setLevel(Level.valueOf(jsonLogger.getLevel()));
        return  JsonResult.success();

    }
}
