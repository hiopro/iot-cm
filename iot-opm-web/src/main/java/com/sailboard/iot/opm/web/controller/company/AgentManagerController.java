package com.sailboard.iot.opm.web.controller.company;

import javax.servlet.http.HttpServletRequest;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.DataGrid;
import com.sailboard.iot.opm.dao.entity.Agent;
import com.sailboard.iot.opm.service.company.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author: liyl
 * @date: 2017/11/5 上午10:38
 * @since 1.0.0
 *
 * 代理商管理
 */

//@RestController
@RequestMapping("/v1/api0/")
public class AgentManagerController {

    @Autowired
    private AgentService agentService;

    @RequestMapping(value = "agent/list")
    public DataGrid<Agent> userList(Integer page, Integer rows, Agent agent) {
        page = page == null ? 1 : page;
        rows = rows == null ? 10 : rows;
        PageInfo pageInfo = agentService.getList(page, rows, agent);
        return new DataGrid(pageInfo);
    }

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public ModelAndView test(HttpServletRequest request) {
        ModelAndView modelAndView= new ModelAndView("index");
        modelAndView.addObject("t","123");
        System.out.println(request.getContextPath());
        return modelAndView;
    }
}
