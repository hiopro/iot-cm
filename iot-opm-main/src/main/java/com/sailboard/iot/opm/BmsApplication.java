package com.sailboard.iot.opm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sailboard.iot.opm.web.controller.GetFile;
import com.sailboard.iot.opm.web.controller.GetImage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tiansj on 15/3/27.
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class BmsApplication {
    @Bean
    public ServletRegistrationBean testServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new GetImage());
        registration.addUrlMappings("/GetImage");
        return registration;
    }
    @Bean
    public ServletRegistrationBean testServletRegistrationFile() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new GetFile());
        registration.addUrlMappings("/GetFile");
        return registration;
    }

    public static ConfigurableApplicationContext run(Object source, String[] args) {
        return run(new Object[] { source }, args);
    }

    public static ConfigurableApplicationContext run(Object[] sources, String[] args) {
        List<Object> sourcesExt = new ArrayList<>(Arrays.asList(sources));
        SpringApplication app = new SpringApplication(sourcesExt.toArray());
        return app.run(args);
    }

    public static void main(String[] args) throws InterruptedException {
        run(BmsApplication.class, args);
    }
}
