#!/bin/sh

# 1. download new deploy package

echo "1. download new deploy package"
if [ $# -ne 1 ] ; then
   echo "param error: need input param deploy filename"
   exit 1
fi

filename=$1

downloadUrl="http://172.17.125.174:8081/nexus/content/repositories/snapshots/com/sailboard/iot/iot-opm-main/1.0.0-SNAPSHOT/${filename}"
echo "wget url : $downloadUrl"

status=`curl --connect-timeout 1 -s -w "%{http_code}" -o temp $downloadUrl`
if [ $status -ne 200 ] ; then
   echo "param error: url not available, please check param filename"
   exit 1
fi

wget $downloadUrl
echo -e "done\r\n"


# 2. remove old iot-opm

echo "2. remove old iot-opm"
rm -rf /home/iot-opm-main
echo -e "done\r\n"


# 3. unzip new deploy package

echo "3. unzip new deploy package"
unzip /home/$filename -d /home
echo -e "done\r\n"


# 4. kill nginx (负载均衡监控的是nginx，不能先起服务，kill nginx 后自动负载到另外一台Server上)

echo "4. kill nginx"
killall -9 nginx
echo -e "done\r\n"

# 5. start iot-opm

echo "5. start online 8088 iot-opm"
/home/iot-opm-main/bin/start.sh online 8088
echo -e "done\r\n"

# 6. start nginx

echo "6. start nginx"

# 每秒检查一次8088端口，server启动后，再启动nginx
while [ true ]; do

    /bin/sleep 1

    str=`netstat -tln | grep 8088 | awk -F ' ' '{print $1}'`
    if [ ! -n "$str" ]; then
        echo "iot-opm is starting..."
    else
        echo -e "iot-opm has been started"
        /usr/local/nginx/sbin/nginx
        echo -e "done\r\n"
        break
    fi

done

echo -e "deploy success\r\n"

exit 0

