﻿function createUploadify(uploadObj){
	var buttonImage='/js/uploadify-v3.2.1/img/addImg.jpg';
	if(uploadObj.buttonType=="text"){
		buttonImage=null;
	}
	var isMulti=false;
	if(uploadObj.isMulti!=undefined){
		isMulti=uploadObj.isMulti;
	}
	setTimeout(function(){
	$("#"+uploadObj.uploadId).uploadify({
		//flash
		'swf': '/js/uploadify-v3.2.1/uploadify.swf',
		//上传处理程序
		'uploader':'/vi/upload',
		//按钮的背景图片，默认为NULL
		'buttonImage':buttonImage,
		//额外增加的上传按钮样式类型
		//'buttonClass':'',
		//上传按钮Hover时的鼠标形状，默认值是’hand’
		'buttonCursor':'hand',
		//按钮上显示的文字，默认”SELECT FILES”
		'buttonText':uploadObj.buttonText,
		//默认是false，若要检查可指明一个用于判断的脚本的路径
		'checkExisting':false,
		//开启调试
		'debug' : false,
		//是否自动上传
		'auto':true,
		//服务器端脚本使用的文件对象的名称 $_FILES个['upload']
		'fileObjName':uploadObj.fileObjName,
		//上传文件的大小限制
		'fileSizeLimit':uploadObj.fileSizeLimit,
		//在浏览窗口底部的文件类型下拉菜单中显示的文本
		'fileTypeDesc':'支持的格式：',
		//允许上传的文件后缀
		'fileTypeExts':uploadObj.fileTypeExts,
		//浏览按钮的宽度
		'width':'80',
		//浏览按钮的高度
		'height':'22',
		//默认是’post’,也可以设置为’get’
		'method':'post',
		//是否支持多文件上传，默认为true 
		'multi':isMulti,
		//不执行默认的onSelect事件,Uploadify插件里面有一些事件，该参数意义就是设置哪些事件可以被用户 覆写
		'overrideEvents' : ['onDialogClose'],
		//若设置为true，一个随机数将被加载swf文件URL的后面，防止浏览器缓 存。默认值为true
		'preventCaching':true,
		//设置文件上传时显示的数据，有两个选择：‘上传速度‘或者’百分比‘， 分别对应’speed’和’percentage’
		'progressData':'speed',
		//文件选择后的容器ID
		'queueID':'null',
		//上传数量, 队列长度限制，缺省值999
		'queueSizeLimit' : 1,
		//最多上传文件数量，默认999
		'uploadLimit':1,
		//表示在上传完成后是否删除队列中的对应元素。默认是True，即上传完成后就看不到上传文件进度条了。 
		'removeCompleted':true,
		//表示上传完成后多久删除队列中的进度条，默认为3，即3秒。
		'removeTimeout':3,
		//若设置为True，那么在上传过程中因为出错导致上传失败的文件将被重新加 入队列。
		'requeueErrors':true,
		//表示文件上传完成后等待服务器响应的时间。超过该时间，那么将认为上传 成功。默认是30，表示30秒。
		'successTimeout':99999,
		//附带值
		'formData':{
		 'bType':uploadObj.uploadType,
		 'elementName':uploadObj.fileObjName
		},
		//每次初始化一个队列时触发
		'onInit':function(instance){
			//alert('The queue ID is'+instance.settings.queueID);
		},
		//每次更新上载的文件的进展
		'onUploadProgress' : function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
			//有时候上传进度什么想自己个性化控制，可以利用这个方法
			//使用方法见官方说明
		},
		//在文件被移除出队列时触发
		'onCancel':function(file){
			
		},
		//在调用cancel方法且传入参数’*’时触发
		'onClearQueue':function(queueItemCount){
		},
		//调用destroy方法时触发
		'onDestroy':function(){
			
		},
		//打开文件对话框关闭时触发
		'onDialogClose':function(queueData){
			//queueData的属性：filesSelected，filesQueued，filesReplaced，filesCancelled，filesErrored
		},
		//选择文件对话框打开时触发。
		'onDialogOpen':function(){
			
		},
		//禁用Uploadify时触发（通过disable方法）
		'onDisable':function(){
			
		},
		//使能Uploadift时触发
		'onEnable':function(){
			
		},
		//在队列中的文件上传完成后触发
		'onQueueComplete':function(queueData){
			
		},
		//选择上传文件后调用
		'onSelect' : function(file) {
			if($("#uploadLoadingImage")){
				$("#uploadLoadingImage").show();
			}
		},
		//返回一个错误，选择文件的时候触发
		'onSelectError':function(file, errorCode, errorMsg){
			switch(errorCode) {
			case -100:
				top.window.showMessage( "上传的文件数量已经超出系统限制的"+$('#'+uploadObj.uploadId).uploadify('settings','queueSizeLimit')+"个文件！");
				break;
			case -110:
				top.window.showMessage( "文件 ["+file.name+"] 大小超出系统限制的"+$('#'+uploadObj.uploadId).uploadify('settings','fileSizeLimit')+"大小！");
				break;
			case -120:
				top.window.showMessage( "文件 ["+file.name+"] 大小异常！");
				break;
			case -130:
				top.window.showMessage( "文件 ["+file.name+"] 类型不正确！");
				break;
			}
		},
		//FLASH对象加载成功后触发
		'onSWFReady':function(){
			
		},
		//检测FLASH失败调用
		'onFallback':function(){
			top.window.showMessage("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试。");
		},
		//上传文件成功后触发（每一个文件都触发一次） 
		'onUploadComplete':function(file){
		},
		//上传文件失败触发，参数如下：
		'onUploadError':function(file,errorCode,errorMsg,errorString){
		},
		//每个文件上传后更新一次进度信息。
		'onUploadProgress':function(file,bytesUploaded,bytesTotal,totalBytesUploaded,totalBytesTotal){
		},
		//在一个文件开始上传之前触发。
		'onUploadStart':function(file){
		},
		//上传到服务器，服务器返回相应信息到data里
		'onUploadSuccess':function(file, data, response){
		  var obj=eval('(' + data+ ')');
			if(obj.code==200||obj.code=='200'||obj.code=="200"){
				obj.resultMap.imageUrl=obj.resultMap.imageUrl.replace(new RegExp("\\\\","g"),"/");
				var  method=eval(uploadObj.uploadImgMethod);
				new method(obj);
			}else{
				top.window.showMessage("上传的文件格式非法！")
			}
		}
	});
	},10);
}

//uploadify的方法
//1、cancel(fileID, suppressEvent)
	//取消队列中的任务，不管此任务是否已经开始上传
	//fileID – 要取消的文件ID，如果为空则取消队列中第一个任务，如果为’*'则取消所有任务
	//suppressEvent – 是否阻止触发onUploadCancel事件，当清空队列时非常实用。
	//取消第一个
		//$("#file_upload").uploadify("cancel");
	//清空队列
		//$("#file_upload").uploadify("cancel", "*");
//2、destroy()
	//销毁Uploadify实例并将文件上传按钮恢复到原始状态
	//销毁Uploadify实例
		//$("#file_upload").uploadify("destroy");
//3、disable(setDisabled)
	//禁用或启用文件浏览按钮
	//setDisabled – 设置为true表示禁用，false为启用
	//禁用按钮
		//$("#file_upload").uploadify("disable", true);
	//启用按钮
		//$(""#file_upload").uploadify("disable", false)
//4、settings(name, value, resetObjects)
	//获取或设置Uploadify实例参数
	//name – 属性名称，如果只提供属性名称则表示获取其值
	//value – 属性值
	//resetObjects – 设置为true时，更新postData对象将清空现有的值。否则，新的值将被添加到其末尾。
	//$("#file_upload").uploadify("settings","buttonText","BROWSE");
	//$("#file_upload").uploadify("settings","buttonText"));
//5、stop()
	//停止当前正在上传的任务
	//停止上传
		//$("#file_upload").uploadify("stop");
	//开始上传所有任务
//6、upload(fileID)
	//立即上传指定的文件，如果fileID为’*'表示上传所有文件，要指定上传多个文件，则将每个文件的fileID作为一个参数
	//开始上传所有文件
		// $("#file_upload").uploadify("upload", "*");