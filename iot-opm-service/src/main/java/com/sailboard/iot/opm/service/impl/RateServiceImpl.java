package com.sailboard.iot.opm.service.impl;

import com.sailboard.iot.opm.dao.entity.Rate;
import com.sailboard.iot.opm.dao.mapper.RateMapper;
import com.sailboard.iot.opm.service.RateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RateServiceImpl implements RateService {
    /**
     * 平台公司获得当前代理商的费率设置
     *
     * @param companyID
     * @return
     */
    @Resource
    RateMapper rateMapper;
    @Override
    public Rate getRateByCompanyID(Long companyID) {
        return rateMapper.selectByAgentID(companyID);
    }

    @Override
    public boolean updateRateSelective(Rate rate) {
        return rateMapper.updateByPrimaryKeySelective(rate)==1;
    }

    @Override
    public boolean insertRateSelective(Rate rate) {
        return rateMapper.insertSelective(rate)==1;
    }

    @Override
    public Rate getRateByPartnerID(Long partnerId){return rateMapper.getRateByPartnerId(partnerId);}

}
