package com.sailboard.iot.opm.service.equipment;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.*;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.service.equipment
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/25 20:44
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/25 20:44
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
public interface EquipmentConfigService {
    PageInfo getRechargeCardList(int pageNum, int pageSize, RechargeCard rechargeCard);
    boolean addRechargeCard(RechargeCard rechargeCard);
    boolean deleteRechargeCard(Long rechargeCardId);
    boolean batchDelRechargeCard(String ids);
    boolean updateRechargeCard(RechargeCard rechargeCard);
    boolean updateRechargeCardStatus(Long id,Integer status);

    PageInfo getChargeStandardList(int pageNum, int pageSize, ChargeStandard chargeStandard);
    boolean addChargeStandard(ChargeStandard chargeStandard);
    boolean deleteChargeStandard(Long ChargeStandardId);
    boolean updateChargeStandard(ChargeStandard chargeStandard);
    boolean updateChargeStandardStatus(Long id, Integer status);
    boolean batchDelChargeStandard(String ids);

    PageInfo getChargeMethodList(int pageNum, int pageSize, ChargeMethod chargeMethod);
    boolean addChargeMethod(ChargeMethod chargeMethod);
    boolean deleteChargeMethod(Long chargeMethodId);
    boolean updateChargeMethod(ChargeMethod chargeMethod);
    boolean updateChargeMethodStatus(Long id, Integer status);
    boolean batchDelChargeMethod(String ids);

    PageInfo getConsoleChargeStandardList(int pageNum,int pageSize,ConsoleChargeStandard consoleChargeStandard);
    boolean addConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard);
    boolean updateConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard);
    boolean batchDelConsoleChargeStandard(String ids);
    boolean updateConsoleChargeStandardStatus(Long id, Integer status);
}
