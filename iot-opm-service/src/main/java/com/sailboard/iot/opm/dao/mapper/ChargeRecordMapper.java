package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ChargeRecord;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface ChargeRecordMapper {
    @Delete({
        "delete from charge_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into charge_record (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "start_time, end_time, ",
        "terminal_id, charge_time, ",
        "costs, station_name, ",
        "terminal_number, current_power, ",
        "average_power, remarks, ",
        "charger_id, charger_name)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{startTime,jdbcType=TIMESTAMP}, #{endTime,jdbcType=TIMESTAMP}, ",
        "#{terminalId,jdbcType=BIGINT}, #{chargeTime,jdbcType=DECIMAL}, ",
        "#{costs,jdbcType=DECIMAL}, #{stationName,jdbcType=VARCHAR}, ",
        "#{terminalNumber,jdbcType=VARCHAR}, #{currentPower,jdbcType=DECIMAL}, ",
        "#{averagePower,jdbcType=DECIMAL}, #{remarks,jdbcType=VARCHAR}, ",
        "#{chargerId,jdbcType=BIGINT}, #{chargerName,jdbcType=VARCHAR})"
    })
    int insert(ChargeRecord record);

    int insertSelective(ChargeRecord record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "start_time, end_time, terminal_id, charge_time, costs, station_name, terminal_number, ",
        "current_power, average_power, remarks, charger_id, charger_name",
        "from charge_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ChargeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChargeRecord record);

    @Update({
        "update charge_record",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "start_time = #{startTime,jdbcType=TIMESTAMP},",
          "end_time = #{endTime,jdbcType=TIMESTAMP},",
          "terminal_id = #{terminalId,jdbcType=BIGINT},",
          "charge_time = #{chargeTime,jdbcType=DECIMAL},",
          "costs = #{costs,jdbcType=DECIMAL},",
          "station_name = #{stationName,jdbcType=VARCHAR},",
          "terminal_number = #{terminalNumber,jdbcType=VARCHAR},",
          "current_power = #{currentPower,jdbcType=DECIMAL},",
          "average_power = #{averagePower,jdbcType=DECIMAL},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
          "charger_id = #{chargerId,jdbcType=BIGINT},",
          "charger_name = #{chargerName,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ChargeRecord record);
}