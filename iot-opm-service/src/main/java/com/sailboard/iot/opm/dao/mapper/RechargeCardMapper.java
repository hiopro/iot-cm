package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.RechargeCard;
import com.sailboard.iot.opm.dao.entity.Terminal;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface RechargeCardMapper {
    @Delete({
        "delete from recharge_card",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into recharge_card (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "station_id, face_value, ",
        "free_value, expiration_date, ",
        "remarks)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{stationId,jdbcType=BIGINT}, #{faceValue,jdbcType=BIGINT}, ",
        "#{freeValue,jdbcType=BIGINT}, #{expirationDate,jdbcType=DATE}, ",
        "#{remarks,jdbcType=VARCHAR})"
    })
    int insert(RechargeCard record);

    int insertSelective(RechargeCard record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "station_id, face_value, free_value, expiration_date, remarks",
        "from recharge_card",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    RechargeCard selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RechargeCard record);

    @Update({
        "update recharge_card",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "station_id = #{stationId,jdbcType=BIGINT},",
          "face_value = #{faceValue,jdbcType=BIGINT},",
          "free_value = #{freeValue,jdbcType=BIGINT},",
          "expiration_date = #{expirationDate,jdbcType=DATE},",
          "remarks = #{remarks,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(RechargeCard record);

    /*自定义*/
    List<RechargeCard> getList(Map map);

    @Update({
            "update recharge_card",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDelete(List<Long> ids);
}