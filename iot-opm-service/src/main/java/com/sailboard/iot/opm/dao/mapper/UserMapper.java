package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    @Delete({
        "delete from sys_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into sys_user (id, username, ",
        "password, salt, ",
        "status, real_name, ",
        "remark, email, create_time, ",
        "update_time, company_id, ",
        "company_name,department_id,department_name)",
        "values (#{id,jdbcType=BIGINT}, #{username,jdbcType=VARCHAR}, ",
        "#{password,jdbcType=VARCHAR}, #{salt,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{realName,jdbcType=VARCHAR}, ",
        "#{remark,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{companyId,jdbcType=BIGINT}, ",
        "#{companyName,jdbcType=VARCHAR},#{departmentID,jdbcType=BIGINT},#{departmentName,jdbcType=VARCHAR},#{mobile,jdbcType=VARCHAR})"
    })
    int insert(User record);

    int insertSelective(User record);

    @Select({
        "select",
        "id, username, password, salt, status, real_name, remark, email, create_time, ",
        "update_time, company_id, company_name, department_id, department_name,mobile",
        "from sys_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    @Update({
        "update sys_user",
        "set username = #{username,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "salt = #{salt,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "real_name = #{realName,jdbcType=VARCHAR},",
          "remark = #{remark,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "company_id = #{companyId,jdbcType=BIGINT},",
          "company_name = #{companyName,jdbcType=VARCHAR}",
          "department_id = #{department_id,jdbcType=BIGINT},",
          "department_name = #{department_name,jdbcType=VARCHAR},",
            "mobile = #{mobile,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(User record);


    List<User> getUserList(Map map);

    @Select({
            "select",
            "id, username, password, salt, status, real_name, remark, email, create_time, update_time,company_id,company_name,department_id,department_name,mobile",
            "from sys_user",
            "where username = #{username,jdbcType=VARCHAR}"
    })
    @ResultMap("BaseResultMap")
    User getByUsername(String username);

    int batchDelUser(List<Long> ids);

    @Update({
            "update sys_user",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateUserStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDisableUser(List<Long> ids);
    int batchEnableUser(List<Long> ids);

    //新增,根据电话排重
    @Select({
            "select * from sys_user where mobile = #{mobile,jdbcType=VARCHAR}"
    })
    User getUserByMobile(@Param("mobile")String mobile);


    //编辑根据用户名、电话排重
    @Select({
            "select * from sys_user where username = #{username,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT}"
    })
    User getOtherUserByName(@Param("username")String username,@Param("id")Long id);

    @Select({
            "select * from sys_user where mobile = #{mobile,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT}"
    })
    User getOtherUserByMobile(@Param("mobile")String mobile,@Param("id")Long id);

    @Select({
            "select *",
            "from sys_user",
            "where username = #{username,jdbcType=VARCHAR} or mobile = #{mobile,jdbcType=VARCHAR}"
    })
    User getUserByUserNameOrMobile(@Param("username")String username,@Param("mobile")String mobile);


}