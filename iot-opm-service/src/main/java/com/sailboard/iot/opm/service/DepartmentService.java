package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Department;

import java.util.List;

public interface DepartmentService {
    PageInfo getDepartmentList(int pageNum, int pageSize, Department department);
    boolean addDepartment(Department department);
    boolean updateDepartment(Department department);
    boolean deleteDepartment(Long departmentID);
    boolean batchDeleteDepartment(String ids);
    boolean updateDepartmentStatus(Long id, Integer status);
    List<Department> getUserDepartment(Long userId);
    Department getDepartmentById(Long departmentId);

    //名称去重。仅限本公司内。
    Department getDepartmentByName(String name,Long companyId);
    Department getOtherDepartmentByName(String name,Long id,Long companyId);
}
