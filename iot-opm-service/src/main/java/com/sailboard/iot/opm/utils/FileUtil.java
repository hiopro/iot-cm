package com.sailboard.iot.opm.utils;

import java.io.*;
import java.net.URL;
import java.util.Date;

public class FileUtil {
	public static void rmdirs(File path) {
		try {
			if (path.isFile()) {
				path.delete();
				return;
			}
			File[] subs = path.listFiles();
			if (subs.length == 0) {
				path.delete();
			} else {
				for (int i = 0; i < subs.length; i++) {
					rmdirs(subs[i]);
				}
				path.delete();
			}
		} catch (Exception ex) {
		}
	}

	public static void xcopy(File src, File destine) {
		try {
			if ((!src.exists())
					|| (src.getCanonicalPath().equals(destine
							.getCanonicalPath())))
				return;
		} catch (IOException ex) {
		}
		File[] chs = src.listFiles();
		for (int i = 0; i < chs.length; i++)
			if (chs[i].isFile()) {
				File destineFile = new File(destine, chs[i].getName());
				copy(chs[i], destineFile);
			} else {
				File destineDir = new File(destine, chs[i].getName());
				destineDir.mkdirs();
				xcopy(chs[i], destineDir);
			}
	}

	public static void move(File src, File destine) {
		try {
			if ((!src.exists())
					|| (src.getCanonicalPath().equals(destine
							.getCanonicalPath())))
				return;
		} catch (IOException ex) {
		}
		copy(src, destine);
		src.delete();
	}

	public static void deleteDirectory(File directory) throws IOException {
		if (!directory.exists()) {
			return;
		}

		cleanDirectory(directory);
		if (!directory.delete()) {
			String message = "Unable to delete directory " + directory + ".";
			throw new IOException(message);
		}
	}

	public static void forceMkdir(File directory) throws IOException {
		if (directory.exists()) {
			if (directory.isFile()) {
				String message = "File " + directory + " exists and is "
						+ "not a directory. Unable to create directory.";

				throw new IOException(message);
			}
		} else if (false == directory.mkdirs()) {
			String message = "Unable to create directory " + directory;
			throw new IOException(message);
		}
	}

	public static void copy(File src, File destine) {
		try {
			if ((!src.exists())
					|| (src.getCanonicalPath().equals(destine
							.getCanonicalPath())))
				return;
			FileInputStream fins = new FileInputStream(src);
			copy(fins, destine);
			destine.setLastModified(src.lastModified());
		} catch (Exception e) {
		}
	}

	public static void copy(InputStream fins, File destine) {
		try {
			if (fins == null)
				return;
			destine.getParentFile().mkdirs();
			FileOutputStream fos = new FileOutputStream(destine);
			byte[] buf = new byte[1024];
			int readLen;
			while ((readLen = fins.read(buf, 0, buf.length)) > 0) {
				fos.write(buf, 0, readLen);
			}
			fos.flush();
			fos.close();
			fins.close();
		} catch (Exception ex) {
		}
	}

	public static long sizeOfDirectory(File directory) {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		long size = 0L;

		File[] files = directory.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (file.isDirectory())
				size += sizeOfDirectory(file);
			else {
				size += file.length();
			}
		}

		return size;
	}

	public static boolean isFileNewer(File file, File reference) {
		if (reference == null) {
			throw new IllegalArgumentException("No specified reference file");
		}
		if (!reference.exists()) {
			throw new IllegalArgumentException("The reference file '" + file
					+ "' doesn't exist");
		}

		return isFileNewer(file, reference.lastModified());
	}

	public static boolean isFileNewer(File file, Date date) {
		if (date == null) {
			throw new IllegalArgumentException("No specified date");
		}
		return isFileNewer(file, date.getTime());
	}

	public static boolean isFileNewer(File file, long timeMillis) {
		if (file == null) {
			throw new IllegalArgumentException("No specified file");
		}
		if (!file.exists()) {
			return false;
		}

		return file.lastModified() > timeMillis;
	}

	public static File toFile(URL url) {
		if (!url.getProtocol().equals("file")) {
			return null;
		}
		String filename = url.getFile().replace('/', File.separatorChar);
		return new File(filename);
	}

	public static URL[] toURLs(File[] files) throws IOException {
		URL[] urls = new URL[files.length];
		for (int i = 0; i < urls.length; i++) {
			urls[i] = files[i].toURL();
		}

		return urls;
	}

	public static void cleanDirectory(File directory) throws IOException {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		IOException exception = null;

		File[] files = directory.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			try {
				forceDelete(file);
			} catch (IOException ioe) {
				exception = ioe;
			}
		}

		if (null != exception)
			throw exception;
	}

	public static void forceDelete(File file) throws IOException {
		if (file.isDirectory()) {
			deleteDirectory(file);
		} else {
			if (!file.exists()) {
				throw new FileNotFoundException("File does not exist: " + file);
			}
			if (!file.delete()) {
				String message = "Unable to delete file: " + file;
				throw new IOException(message);
			}
		}
	}

	public static void forceDeleteOnExit(File file) throws IOException {
		if (file.isDirectory())
			deleteDirectoryOnExit(file);
		else
			file.deleteOnExit();
	}

	private static void deleteDirectoryOnExit(File directory)
			throws IOException {
		if (!directory.exists()) {
			return;
		}

		cleanDirectoryOnExit(directory);
		directory.deleteOnExit();
	}

	private static void cleanDirectoryOnExit(File directory) throws IOException {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		IOException exception = null;

		File[] files = directory.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			try {
				forceDeleteOnExit(file);
			} catch (IOException ioe) {
				exception = ioe;
			}
		}

		if (null != exception)
			throw exception;
	}
	
	public static void deleteAndBankDir(File file) {
		try {
			if(file.exists()){
				File parentFile=file.getParentFile();
				file.delete();
				if(parentFile.listFiles().length==0){
					deleteAndBankDir(parentFile);
				}
			}
		} catch (Exception ex) {
		}
	}

}
