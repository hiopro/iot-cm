package com.sailboard.iot.opm.service.aliyun;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.sailboard.iot.opm.common.Constants;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.RedisConstants;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.common.domain.VerifyType;
import com.sailboard.iot.opm.config.AliyunConfig;
import com.sailboard.iot.opm.config.VerifyCodeConfig;
import com.sailboard.iot.opm.dao.entity.Sms;
import com.sailboard.iot.opm.dao.mapper.SmsMapper;
import com.sailboard.iot.opm.framework.RedisManager;
import com.sailboard.iot.opm.utils.StringUtils;
import com.sailboard.iot.opm.utils.Utils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class SmsService {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(SmsService.class);

    @Resource
    RedisManager redisManager;
    @Resource
    VerifyCodeConfig verifyCodeConfig;
    @Resource
    AliyunConfig aliyunConfig;
    @Resource
    SmsMapper smsMapper;

    // 业务设置
    public static final Integer IP_MAX_SMS_NUMBER = 100; //每日最大IP申请数

    // 阿里云设置
    static final String product = "Dysmsapi"; //产品名称:云通信短信API产品,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com"; //产品域名,开发者无需替换

    /**
     * 发生手机验证码
     * @param mobile
     * @return
     */
    public JsonResult sendSms(String mobile, String ip) {
        if (StringUtils.isBlank(mobile)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "请输入手机号");
        }
        if (!Utils.checkMobile(mobile)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, "请核对您的手机号");
        }
        // 已发送，不能频繁调用，1分钟后redis自动失效
        if (!verifyCodeConfig.getIsTest() && redisManager.get(RedisConstants.MOBILE + mobile) != null) {
            return new JsonResult(ResultCode.VERIFY_CODE_ERROR_CODE, ResultCode.VERIFY_CODE_OFTEN_MSG);
        }
        //记录发送短信IP，单日最高100次
        String ipNumber = redisManager.get(RedisConstants.IP_MAX_SMS + ip);
        logger.info("redisInfo:{}", RedisConstants.IP_MAX_SMS + ip);
        if (ipNumber == null) {
            redisManager.set(RedisConstants.IP_MAX_SMS + ip, "1", 86400);
        } else {
            if (Integer.parseInt(ipNumber) >= IP_MAX_SMS_NUMBER) {
                return new JsonResult(ResultCode.VERIFY_CODE_ERROR_CODE, ResultCode.VERIFY_CODE_LIMIT_ERROR_MSG);
            }
            Integer nNumber = Integer.parseInt(ipNumber) + 1;
            redisManager.set(RedisConstants.IP_MAX_SMS + ip, nNumber.toString());
        }
        // 生成验证码
        String code = verifyCodeConfig.generateCode(VerifyType.NUMBER);
        if(verifyCodeConfig.getIsTest()) { // 测试过程，code=111111，不发送到真机，直接存储并返回
            return insert(mobile, code);
        }  else { // 发送手机号
            try {
                JsonResult result = insert(mobile, code);
                if (result.getCode() == ResultCode.SUCCESS_CODE) {
                    SendSmsResponse sendSmsResponse = sendSms2Phone(mobile, code);
                    if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
                        logger.debug("send sms success, mobile = {}", mobile);
                    } else {
                        logger.error("send sms failed, mobile = {}, errorCode = {}, errorMsg = {}", mobile, sendSmsResponse.getCode(), sendSmsResponse.getMessage());
                        result = new JsonResult(ResultCode.VERIFY_CODE_ERROR_CODE, ResultCode.VERIFY_CODE_SEND_ERROR_MSG);
                    }
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("send sms failed, mobile = {}, exception msg = {}", mobile, e.getMessage());
                return new JsonResult(ResultCode.VERIFY_CODE_ERROR_CODE, ResultCode.VERIFY_CODE_SEND_ERROR_MSG);
            }
        }
    }

    /**
     * 调用阿里云服务发送验证码
     * @param mobile
     * @param code
     * @return
     * @throws ClientException
     */
    public SendSmsResponse sendSms2Phone(String mobile, String code) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
//        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
                aliyunConfig.getSms().getAccessKeyId(), aliyunConfig.getSms().getAccessKeySecret());
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号
        request.setPhoneNumbers(mobile);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(aliyunConfig.getSms().getSignName());
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(aliyunConfig.getSms().getTemplateCode());
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        // request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"123\"}");
        request.setTemplateParam("{\"number\":\""+code+"\"}");

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        // request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        // request.setOutId("yourOutId");    //"yourOutId"

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

        return sendSmsResponse;
    }


    public QuerySendDetailsResponse querySendDetails(String bizId, String mobile) throws ClientException {
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",
                aliyunConfig.getSms().getAccessKeyId(), aliyunConfig.getSms().getAccessKeySecret());
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber(mobile);
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);

        //hint 此处可能会抛出异常，注意catch
        QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);

        return querySendDetailsResponse;
    }

    public JsonResult insert(String mobile, String code) {
        Date sendTime = new Date();
        Date expireTime = DateUtils.addSeconds(sendTime, 60);
        Sms sms = new Sms();
        sms.setMobile(mobile);
        sms.setCode(code);
        sms.setSendTime(sendTime);
        sms.setExpireTime(expireTime);
        sms.setType(Constants.Sms.TYPE_MOBILE);
        int row = smsMapper.insert(sms);
        if(row > 0) {
            redisManager.set(RedisConstants.MOBILE + sms.getMobile(), sms.getCode(), 60);
        }
        return row > 0 ? JsonResult.success() : JsonResult.error();
    }

    public String getVerifyCodeByMobile(String mobile) {
        if(StringUtils.isBlank(mobile)) {
            return null;
        }
        String code = redisManager.get(RedisConstants.MOBILE + mobile);
        if(StringUtils.isBlank(code)) {
            code = smsMapper.selectVerifyCodeByMobile(mobile);
        }
        return code;
    }

//    public static void main(String[] args) throws ClientException, InterruptedException {
//
//        String mobile = "13521468432";
//        String code = "111111";
//        //发短信
//        SendSmsResponse response = sendSms(mobile, code);
//        System.out.println("短信接口返回的数据----------------");
//        System.out.println("Code=" + response.getCode());
//        System.out.println("Message=" + response.getMessage());
//        System.out.println("RequestId=" + response.getRequestId());
//        System.out.println("BizId=" + response.getBizId());
//
//        Thread.sleep(3000L);
//
//        //查明细
//        if(response.getCode() != null && response.getCode().equals("OK")) {
//            QuerySendDetailsResponse querySendDetailsResponse = querySendDetails(response.getBizId(), mobile);
//            System.out.println("短信明细查询接口返回数据----------------");
//            System.out.println("Code=" + querySendDetailsResponse.getCode());
//            System.out.println("Message=" + querySendDetailsResponse.getMessage());
//            int i = 0;
//            for(QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs())
//            {
//                System.out.println("SmsSendDetailDTO["+i+"]:");
//                System.out.println("Content=" + smsSendDetailDTO.getContent());
//                System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
//                System.out.println("OutId=" + smsSendDetailDTO.getOutId());
//                System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
//                System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
//                System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
//                System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
//                System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
//            }
//            System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
//            System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
//        }
//
//    }

}
