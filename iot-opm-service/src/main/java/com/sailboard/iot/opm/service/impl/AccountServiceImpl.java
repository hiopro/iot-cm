package com.sailboard.iot.opm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Account;
import com.sailboard.iot.opm.dao.entity.Agent_Account;
import com.sailboard.iot.opm.dao.mapper.AccountMapper;
import com.sailboard.iot.opm.service.AccountService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
@Service
public class AccountServiceImpl implements AccountService {
    @Resource AccountMapper accountMapper;
    @Override
    public PageInfo getAgent_AccountList(int pageNum, int pageSize, Agent_Account agent_account) {
        PageHelper.startPage(pageNum, pageSize);
        List<Agent_Account> list = accountMapper.getCompany_Agent_Acconut_List(BeanMapUtils.toMap(agent_account));
        return new PageInfo(list);
    }

    @Override
    public boolean batchDisableCompany(String ids) {
        List<Long> list = new ArrayList<>();
        String[] arr = ids.split(",");
        for(String id : arr) {
            long accountId = Long.valueOf(id);
            if(accountId >= 0) {  // 10以下id为初始化
                list.add(Long.valueOf(id));
            }
        }
        return accountMapper.batchDisableAccount(list) > 0;
    }

    @Override
    public boolean updateAccountStatus(Long  accountID, Integer status) {
        return accountMapper.updateAccountStatus(accountID,status)==1;
    }

    @Override
    public boolean addAccount(Account account){
        return accountMapper.insertSelective(account) == 1;
    }

    @Override
    public Account getAccountByName(String name){
        return accountMapper.getAccountByName(name);
    }

    @Override
    public Account getAccountByUserId(Long userId){
        return accountMapper.getAccountByUserId(userId);
    }
    @Override
    public boolean updateAccount(Account account){
        return accountMapper.updateByPrimaryKey(account) == 1;
    }

    @Override
    public Account getAccountByID(Long id) {
        return accountMapper.selectByPrimaryKey(id);
    }


}
