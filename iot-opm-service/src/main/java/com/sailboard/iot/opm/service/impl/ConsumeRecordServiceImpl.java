package com.sailboard.iot.opm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.ConsumeRecord;
import com.sailboard.iot.opm.dao.mapper.ConsumeRecordMapper;
import com.sailboard.iot.opm.service.ConsumeRecordService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ConsumeRecordServiceImpl implements ConsumeRecordService {
    @Resource
    ConsumeRecordMapper consumeRecordMapper;
    @Override
    /**
     * 获得消费记录
     */
    public PageInfo getConsume_RecordList(int pageNum, int pageSize, ConsumeRecord consumeRecord) {
        PageHelper.startPage(pageNum, pageSize);
        List<ConsumeRecord> list = consumeRecordMapper.listConsumeRecord(BeanMapUtils.toMap(consumeRecord));
        return new PageInfo(list);
    }

    /**
     * 更新记录
     * @param consumeRecord
     * @return
     */
    @Override
    public boolean updateConsumeRecordSelective(ConsumeRecord consumeRecord) {
        return consumeRecordMapper.updateByPrimaryKeySelective(consumeRecord)==1;
    }

    @Override
    public boolean updateStatus(Long id, Integer status, String remarks) {
        return consumeRecordMapper.updateRecordtStatus(id, status, remarks)==1;
    }

    @Override
    public PageInfo getWithdrawalApplication(int pageNum, int pageSize,ConsumeRecord consumeRecord){
        PageHelper.startPage(pageNum, pageSize);
        List<ConsumeRecord> list = consumeRecordMapper.getWithdrawalApplication(BeanMapUtils.toMap(consumeRecord));
        return new PageInfo(list);
    }

    public boolean addConsumeRecordWithdrawal(ConsumeRecord consumeRecord){return consumeRecordMapper.insertSelective(consumeRecord) == 1;}

}
