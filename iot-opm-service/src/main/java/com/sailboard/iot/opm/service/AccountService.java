package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Account;
import com.sailboard.iot.opm.dao.entity.Agent_Account;

public interface AccountService {
    PageInfo getAgent_AccountList(int pageNum, int pageSize, Agent_Account agent_account);
    boolean batchDisableCompany(String ids);
    boolean updateAccountStatus(Long  accountid, Integer status);

    boolean addAccount(Account account);
    Account getAccountByName(String name);
    Account getAccountByUserId(Long userId);
    boolean updateAccount(Account account);
    Account getAccountByID(Long id);
}
