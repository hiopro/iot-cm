package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Sms;
import org.apache.ibatis.annotations.*;

public interface SmsMapper {
    @Delete({
        "delete from sms",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into sms (id, mobile, ",
        "code, send_time, ",
        "expire_time, type)",
        "values (#{id,jdbcType=BIGINT}, #{mobile,jdbcType=VARCHAR}, ",
        "#{code,jdbcType=VARCHAR}, #{sendTime,jdbcType=TIMESTAMP}, ",
        "#{expireTime,jdbcType=TIMESTAMP}, #{type,jdbcType=TINYINT})"
    })
    int insert(Sms record);

    int insertSelective(Sms record);

    @Select({
        "select",
        "id, mobile, code, send_time, expire_time, type",
        "from sms",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Sms selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Sms record);

    @Update({
        "update sms",
        "set mobile = #{mobile,jdbcType=VARCHAR},",
          "code = #{code,jdbcType=VARCHAR},",
          "send_time = #{sendTime,jdbcType=TIMESTAMP},",
          "expire_time = #{expireTime,jdbcType=TIMESTAMP},",
          "type = #{type,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Sms record);

    /***********以下为自定义方法************************/

    @Select({
            "select",
            "code",
            "from sms",
            "where mobile = #{mobile,jdbcType=VARCHAR} and expire_time >= now() and type = 1"
    })
    @ResultType(String.class)
    String selectVerifyCodeByMobile(String mobile);

}