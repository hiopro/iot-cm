package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Company;
import com.sailboard.iot.opm.dao.entity.Terminal;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface TerminalMapper {
    @Delete({
        "delete from terminal",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into terminal (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "console_id, serial_number, ",
        "remarks, qr_code, ",
        "qr_code_url,machine_code,business_code)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{consoleId,jdbcType=BIGINT}, #{serialNumber,jdbcType=VARCHAR}, ",
        "#{remarks,jdbcType=VARCHAR}, #{qrCode,jdbcType=VARCHAR}, ",
        "#{qrCodeUrl,jdbcType=VARCHAR},#{machineCode,jdbcType=VARCHAR},#{businessCode,jdbcType=VARCHAR})"
    })
    int insert(Terminal record);

    int insertSelective(Terminal record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "console_id, serial_number, remarks, qr_code, qr_code_url,machine_code,business_code",
        "from terminal",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Terminal selectByPrimaryKey(Long id);

    @Select({
            "select",
            "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
            "console_id, serial_number, remarks, qr_code, qr_code_url,machine_code,business_code",
            "from terminal",
            "where serial_number = #{serialNumber,jdbcType=VARCHAR}"
    })
    @ResultMap("BaseResultMap")
    List<Terminal> selectBySerialNumber(String serialNumber);

    int updateByPrimaryKeySelective(Terminal record);

    @Update({
        "update terminal",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "console_id = #{consoleId,jdbcType=BIGINT},",
          "serial_number = #{serialNumber,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
          "qr_code = #{qrCode,jdbcType=VARCHAR},",
          "qr_code_url = #{qrCodeUrl,jdbcType=VARCHAR},",
            "machine_code = #{machineCode,jdbcType=VARCHAR},",
            "business_code = #{businessCode,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Terminal record);

    @Update({
        "update terminal",
        "set console_id = #{consoleId,jdbcType=BIGINT}",
        "where serial_number = #{serialNumber,jdbcType=VARCHAR}"
    })
    int updateBySerialNumber(@Param("consoleId") Long consoleId, @Param("serialNumber") String serialNumber);

    /*自定义*/
    List<Terminal> getTerminalList(Map map);

    @Update({
            "update terminal",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateTerminalStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDelete(List<Long> ids);

    //代理商查看终端
    List<Terminal> getTerminalListByPartnerId(Map map);
    //终端去重
    Terminal getTerminalByMachineCode(Terminal terminal);
    Terminal getTerminalByBusinessCode(Terminal terminal);
    //批量启用/禁用终端
    int batchDisableTerminal(List<Long> ids);
    int batchEnableTerminal(List<Long> ids);
}