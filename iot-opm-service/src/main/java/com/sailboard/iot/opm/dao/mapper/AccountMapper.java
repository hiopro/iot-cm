package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Account;
import com.sailboard.iot.opm.dao.entity.Agent_Account;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

public interface AccountMapper {
    @Delete({
        "delete from account",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into account (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "type, balance, user_id, ",
        "user_name,recharge,profit,internet remarks,bank_card_number)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{type,jdbcType=TINYINT}, #{balance,jdbcType=DECIMAL}, #{userId,jdbcType=BIGINT}, ",
        "#{userName,jdbcType=VARCHAR}, #{recharge,jdbcType=DECIMAL}, #{profit,jdbcType=DECIMAL}, #{internet,jdbcType=DECIMAL}, #{remarks,jdbcType=VARCHAR}, #{bankCardNumber,jdbcType=VARCHAR})"
    })
    int insert(Account record);

    int insertSelective(Account record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "type, balance, user_id, user_name, remarks, recharge, profit, internet,bank_card_number",
        "from account",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Account selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Account record);

    @Update({
        "update account",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "type = #{type,jdbcType=TINYINT},",
          "balance = #{balance,jdbcType=DECIMAL},",
          "recharge = #{recharge,jdbcType=DECIMAL},",
          "profit = #{profit,jdbcType=DECIMAL},",
          "internet = #{internet,jdbcType=DECIMAL},",
          "user_id = #{userId,jdbcType=BIGINT},",
          "user_name = #{userName,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
            "bank_card_number = #{bankCardNumber,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Account record);

    //列出代理商的账户
    List<Agent_Account> getCompany_Agent_Acconut_List(Map map);

    @Select({
            "select * from account where name = #{name,jdbcType=VARCHAR}"
    })
    @ResultMap("BaseResultMap")
    Account getAccountByName(String name);

    //批量禁用账户
    int batchDisableAccount(List<Long> ids);

    //更新账户状态
    @Update({
            "update account",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateAccountStatus(@Param("id") Long accountID, @Param("status") Integer status);

    //通过userId获取账户信息，userId为公司id。
    @Select({
            "select",
            "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
            "type, balance, user_id, user_name, remarks, recharge, profit, internet,bank_card_number",
            "from account",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Account getAccountByUserId(Long userId);
}