package com.sailboard.iot.opm.service.company;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.dao.entity.Company;
import com.sailboard.iot.opm.dao.entity.CompanySpy;
import com.sailboard.iot.opm.dao.entity.PartnerInfo;
import com.sailboard.iot.opm.dao.entity.User;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/11 20:29
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/11 20:29
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
public interface CompanyService {

    PageInfo getCompanyList(int pageNum, int pageSize, Company company);
    Company getCompanyByID(String companyID);
    boolean addCompany(Company company);
    boolean updateCompany(Company company);
    boolean batchDelCompany(String ids);
    boolean updateCopmanyStatus(Long id, Integer status);
    boolean clearCompanyAgreement(Long id);
    JsonResult batchBindRole(String userIds, String roleIds);
    JsonResult bindRole(Long companyId, String roleIds);
    Company getBySocialCode(String socialCode);
    PageInfo getPartnerInfoList(int page, int rows,  Company company);
    PageInfo getCompany_Agent_Spy_List(int pageNum, int pageSize, CompanySpy company);//列出总的代理商
    CompanySpy getTotal(String companyId);//计算总的代理商和合作伙伴
    PageInfo getCompany_Agent_Spy_PartnerList(int pageNum, int pageSize, Company company);//列出代理商的合作伙伴
    Company getCompanyById(Long CompanyId);
    boolean batchDisableCompany(String ids);//批量禁用公司
    boolean batchEnableCompany(String ids);
    Company getCompanyByName(String name);
    Company getCompanyBySocialCode(String socialCode);

    Company getOtherCompanyByName(String name,String id);//除自身外的公司，编辑用。
    Company getOtherCompanyBySocialCode(String socialCode,String id);

}
