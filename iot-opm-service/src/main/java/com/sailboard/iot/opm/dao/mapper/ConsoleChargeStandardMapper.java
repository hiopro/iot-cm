package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ConsoleChargeStandard;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ConsoleChargeStandardMapper {
    @Delete({
        "delete from console_charge_standard",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into console_charge_standard (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "power_lower, power_upper, ",
        "unit_rice, unit_type, ",
        "terminal_id, gear_sequence, ",
        "console_id)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{powerLower,jdbcType=TINYINT}, #{powerUpper,jdbcType=TINYINT}, ",
        "#{unitRice,jdbcType=DECIMAL}, #{unitType,jdbcType=TINYINT}, ",
        "#{terminalId,jdbcType=BIGINT}, #{gearSequence,jdbcType=TINYINT}, ",
        "#{consoleId,jdbcType=TINYINT})"
    })
    int insert(ConsoleChargeStandard record);

    int insertSelective(ConsoleChargeStandard record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "power_lower, power_upper, unit_rice, unit_type, terminal_id, gear_sequence, ",
        "console_id",
        "from console_charge_standard",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ConsoleChargeStandard selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ConsoleChargeStandard record);

    @Update({
        "update console_charge_standard",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "power_lower = #{powerLower,jdbcType=TINYINT},",
          "power_upper = #{powerUpper,jdbcType=TINYINT},",
          "unit_rice = #{unitRice,jdbcType=DECIMAL},",
          "unit_type = #{unitType,jdbcType=TINYINT},",
          "terminal_id = #{terminalId,jdbcType=BIGINT},",
          "gear_sequence = #{gearSequence,jdbcType=TINYINT},",
          "console_id = #{consoleId,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ConsoleChargeStandard record);


    List getConsoleChargeStandardList(Map map);

    int batchDelete(List list);

    @Update({
            "update console_charge_standard",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);
}