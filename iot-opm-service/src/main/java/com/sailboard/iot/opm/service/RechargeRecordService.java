package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.RechargeRecord;

public interface RechargeRecordService {
    PageInfo getRecharge_RecordList(int pageNum, int pageSize,RechargeRecord rechargeRecord);
}
