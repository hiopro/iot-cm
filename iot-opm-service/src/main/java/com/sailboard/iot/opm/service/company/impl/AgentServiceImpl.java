package com.sailboard.iot.opm.service.company.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Agent;
import com.sailboard.iot.opm.service.company.AgentService;
import org.springframework.stereotype.Service;

/**
 * @author: liyl
 * @date: 2017/11/5 下午4:36
 * @since 1.0.0
 */
@Service
public class AgentServiceImpl implements AgentService {
    @Override
    public PageInfo getList(int pageNum, int pageSize, Agent agent) {
        PageHelper.startPage(pageNum, pageSize);
        Page<Agent> list = new Page();
        //todo 测试代码
        Agent agent1 = new Agent();
        agent1.setId(1L);
        agent1.setAgentName("test1");
        list.add(agent1);
        return new PageInfo(list);
    }
}
