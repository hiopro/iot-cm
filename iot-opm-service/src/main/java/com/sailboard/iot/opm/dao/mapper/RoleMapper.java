
package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Role;
import com.sailboard.iot.opm.dao.entity.User;
import com.sailboard.iot.opm.dao.entity.UserRole;
import org.apache.ibatis.annotations.*;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Map;

public interface RoleMapper {
    @Delete({
        "delete from sys_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into sys_role (id, name, ",
        "status, type, is_manager, create_time, ",
        "update_time)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT},#{type,jdbcType=VARCHAR},#{isManager,jdbcType=VARCHAR},#{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP})"
    })
    int insert(Role record);

    int insertSelective(Role record);

    @Select({
        "select",
        "id, name, status, type, is_manager, create_time, update_time",
        "from sys_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Role selectByPrimaryKey(Long id);


    int updateByPrimaryKeySelective(Role record);

    @Update({
        "update sys_role",
        "set name = #{name,jdbcType=VARCHAR},",
           "type = #{type,jdbcType=VARCHAR},",
            "is_manager = #{isManager,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Role record);

    //------------------------------------

    @Select({
            "select",
            "id, name, status, type, is_manager, create_time, update_time",
            "from sys_role",
            "where type = #{type,jdbcType=VARCHAR}"
    })
    @ResultMap("BaseResultMap")
    List<Role> getRoleList(String type);

    List<Role> getRoleListAll(Map map);

    @Select({
            "select sys_role.* from sys_role, sys_user_role",
            "where sys_role.id = sys_user_role.role_id",
            "and user_id = #{userId,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    List<Role> getUserRoles(Long userId);

    @Select({
            "select sys_role.* from sys_role, company_role",
            "where sys_role.id = company_role.role_id",
            "and company_id = #{companyId,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    List<Role> getCompanyRoles(Long companyId);


    @Update({
            "update sys_role",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateRoleStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDelete(List<Long> ids);

    @Select({
            "select",
            "id, name, status, type, is_manager, create_time, update_time",
            "from sys_role",
            "where is_manager = #{isMgr,jdbcType=VARCHAR} and type = #{type,jdbcType=VARCHAR}"
    })
    Role getRoleByIsMgrAndType(@Param("isMgr")String isMgr,@Param("type")String type);

    @Select({
            "select",
            "id, name, status, type, is_manager, create_time, update_time",
            "from sys_role",
            "where is_manager = #{isMgr,jdbcType=VARCHAR} and type = #{type,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT}"
    })
    Role getOtherRoleByIsMgrAndType(@Param("isMgr")String isMgr,@Param("type")String type,@Param("id")Long id);

}