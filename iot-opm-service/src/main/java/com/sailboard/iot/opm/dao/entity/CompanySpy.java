package com.sailboard.iot.opm.dao.entity;

/**
 * @Author ：Herbort
 * createTime：2018/1/19
 * 用来承接代理商的总体概况
 */
public class CompanySpy {
    private  String companyID;
    private  String companyName;
    private  String totalStation;
    private  String totalPartner;
    private  String companySocialCode;
    private  String companyLeaderName;
    private  String companyMobile;
    private  String companyLeader;
    private  String companyFounder;
    private  String CompanyFounderName;
    private  String companyBelong;
    private  String companyStatus;
    private  String companyType;
    private String agreementPath;
    private String agreementName;
    private Long sequenceNumber;
    private String bankCardNumber;//银行卡号
    private Long contract;
    private String contractPath;
    private String region;
    private Long leader;
    private String leaderName;
    private String businessLicense;
    private String licensePath;
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyLeader() {
        return companyLeader;
    }

    public void setCompanyLeader(String companyLeader) {
        this.companyLeader = companyLeader;
    }

    public String getCompanyFounder() {
        return companyFounder;
    }

    public void setCompanyFounder(String companyFounder) {
        this.companyFounder = companyFounder;
    }

    public String getCompanyFounderName() {
        return CompanyFounderName;
    }

    public void setCompanyFounderName(String companyFounderName) {
        CompanyFounderName = companyFounderName;
    }

    public String getCompanyBelong() {
        return companyBelong;
    }

    public void setCompanyBelong(String companyBelong) {
        this.companyBelong = companyBelong;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTotalStation() {
        return totalStation;
    }

    public void setTotalStation(String totalStation) {
        this.totalStation = totalStation;
    }

    public String getTotalPartner() {
        return totalPartner;
    }

    public void setTotalPartner(String totalPartner) {
        this.totalPartner = totalPartner;
    }

    public String getCompanySocialCode() {
        return companySocialCode;
    }

    public void setCompanySocialCode(String companySocialCode) {
        this.companySocialCode = companySocialCode;
    }

    public String getCompanyLeaderName() {
        return companyLeaderName;
    }

    public void setCompanyLeaderName(String companyLeaderName) {
        this.companyLeaderName = companyLeaderName;
    }

    public String getCompanyMobile() {
        return companyMobile;
    }

    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    public String getAgreementPath() {
        return agreementPath;
    }

    public void setAgreementPath(String agreementPath) {
        this.agreementPath = agreementPath;
    }

    public String getAgreementName() {
        return agreementName;
    }

    public void setAgreementName(String agreementName) {
        this.agreementName = agreementName;
    }

    public Long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber;
    }

    public Long getContract() {
        return contract;
    }

    public void setContract(Long contract) {
        this.contract = contract;
    }

    public String getContractPath() {
        return contractPath;
    }

    public void setContractPath(String contractPath) {
        this.contractPath = contractPath;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Long getLeader() {
        return leader;
    }

    public void setLeader(Long leader) {
        this.leader = leader;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    public String getLicensePath() {
        return licensePath;
    }

    public void setLicensePath(String licensePath) {
        this.licensePath = licensePath;
    }
}
