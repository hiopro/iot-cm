package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.UserStation;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface UserStationMapper {
    @Delete({
        "delete from user_station",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into user_station (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "user_id, user_name, ",
        "station_id, station_name, ",
        "station_address, remarks)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{userId,jdbcType=BIGINT}, #{userName,jdbcType=VARCHAR}, ",
        "#{stationId,jdbcType=BIGINT}, #{stationName,jdbcType=VARCHAR}, ",
        "#{stationAddress,jdbcType=VARCHAR}, #{remarks,jdbcType=VARCHAR})"
    })
    int insert(UserStation record);

    int insertSelective(UserStation record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "user_id, user_name, station_id, station_name, station_address, remarks",
        "from user_station",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    UserStation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserStation record);

    @Update({
        "update user_station",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "user_id = #{userId,jdbcType=BIGINT},",
          "user_name = #{userName,jdbcType=VARCHAR},",
          "station_id = #{stationId,jdbcType=BIGINT},",
          "station_name = #{stationName,jdbcType=VARCHAR},",
          "station_address = #{stationAddress,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(UserStation record);
}