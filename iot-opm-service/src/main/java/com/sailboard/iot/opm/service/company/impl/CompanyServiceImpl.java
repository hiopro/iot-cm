package com.sailboard.iot.opm.service.company.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.common.ResultCode;
import com.sailboard.iot.opm.dao.entity.*;
import com.sailboard.iot.opm.dao.mapper.CompanyMapper;
import com.sailboard.iot.opm.dao.mapper.CompanyRoleMapper;
import com.sailboard.iot.opm.dao.mapper.UserMapper;
import com.sailboard.iot.opm.service.company.CompanyService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import com.sailboard.iot.opm.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.service.company.impl
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/11 20:31
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/11 20:31
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    @Resource
    CompanyMapper companyMapper;
    @Resource
    CompanyRoleMapper companyRoleMapper;
    @Override
    public PageInfo getCompanyList(int pageNum, int pageSize, Company company){
        PageHelper.startPage(pageNum, pageSize);
        List<Company> list = companyMapper.getCompanyList(BeanMapUtils.toMap(company));
        return new PageInfo(list);
    }

    @Override
    public Company getCompanyByID(String companyID) {
        Company company = companyMapper.getCompanyByID(companyID);
        return company;
    }

    @Override
    public PageInfo getPartnerInfoList(int pageNum, int pageSize, Company company){
        PageHelper.startPage(pageNum, pageSize);
        List<PartnerInfo> list = companyMapper.getPartnerInfoList(BeanMapUtils.toMap(company));
        return new PageInfo(list);
    }
    //增加公司
    public boolean addCompany(Company company) {
        return companyMapper.insertSelective(company) == 1;
    }
    //更新公司
    public boolean updateCompany(Company company) {
        return companyMapper.updateByPrimaryKeySelective(company)== 1;
    }
    //批量删除公司
    public boolean batchDelCompany(String ids) {
        List<Long> list = new ArrayList<>();
        String[] arr = ids.split(",");
        for(String id : arr) {
            long companyId = Long.valueOf(id);
            if(companyId >= 0) {
                list.add(Long.valueOf(id));
            }
        }
        return companyMapper.batchDelCompany(list) > 0;
    }

    //更新公司状态
    public boolean updateCopmanyStatus(Long id, Integer status){
        return companyMapper.updateCompanyStatus(id,status)==1;
    }

    //清空合作协议
    public boolean clearCompanyAgreement(Long id){
        return companyMapper.clearCompanyAgreement(id)==1;
    }

    public JsonResult bindRole(Long companyId, String roleIds) {
        try {
            companyRoleMapper.deleteByCompanyId(companyId);
            if(StringUtils.isEmpty(roleIds)) {
                return JsonResult.success();
            }
        } catch (Exception e) {
            return JsonResult.error();
        }
        String[] ids = roleIds.split(",");
        List<CompanyRole> list = new ArrayList<>();
        for(String id : ids) {
            CompanyRole companyRole = new CompanyRole();
            companyRole.setCompanyId(companyId);
            companyRole.setRoleId(Long.valueOf(id));
            list.add(companyRole);
        }
        int row = companyRoleMapper.batchInsert(list);
        return row > 0 ? JsonResult.success() : JsonResult.error();
    }

    public JsonResult batchBindRole(String userIds, String roleIds) {
        if(StringUtils.isEmpty(userIds)) {
            return new JsonResult(ResultCode.PARAM_ERROR_CODE, ResultCode.PARAM_ERROR_MSG);
        }
        List<String> errorIds = new ArrayList<>();
        String[] ids = userIds.split(",");
        for(String id : ids) {
            JsonResult result = bindRole(Long.valueOf(id), roleIds);
            if(result.getCode() != ResultCode.SUCCESS_CODE) {
                errorIds.add(id);
            }
        }
        if(errorIds.size() == 0) {
            return JsonResult.success();
        }
        if(errorIds.size() < ids.length) {
            String str = StringUtils.join(errorIds, ",");
            return new JsonResult(ResultCode.SUCCESS_CODE, "部分用户绑定角色失败，ID=(" +str+ ")");
        } else {
            return new JsonResult(ResultCode.ERROR_CODE, "绑定用户角色失败，请重试");
        }
    }

    public Company getBySocialCode(String socialCode) {
        return companyMapper.getBySocialCode(socialCode);
    }

    @Override
    public PageInfo getCompany_Agent_Spy_List(int pageNum, int pageSize, CompanySpy company) {
        PageHelper.startPage(pageNum, pageSize);
        List<CompanySpy> list = companyMapper.getCompany_Agent_Spy_List(BeanMapUtils.toMap(company));
        return new PageInfo(list);
    }

    @Override
    public CompanySpy getTotal(String companyId) {
        CompanySpy spy = new CompanySpy();
        spy= companyMapper.getTotal(companyId);
        return spy;
    }

    @Override
    public PageInfo getCompany_Agent_Spy_PartnerList(int pageNum, int pageSize, Company company) {
        PageHelper.startPage(pageNum, pageSize);
        List<Company> list = companyMapper.getCompanyList(BeanMapUtils.toMap(company));
        return new PageInfo(list);
    }

    public Company getCompanyById(Long companyId){
        return companyMapper.selectByPrimaryKey(companyId);
    }

    public Company getCompanyByName(String name){
        return companyMapper.getCompanyByName(name);
    }

    public Company getOtherCompanyByName(String name,String id){
        return companyMapper.getOtherCompanyByName(name,id);
    }

    public Company getCompanyBySocialCode(String socialCode){
        return companyMapper.getCompanyBySocialCode(socialCode);
    }

    public Company getOtherCompanyBySocialCode(String socialCode,String id){
        return companyMapper.getOtherCompanyBySocialCode(socialCode,id);
    }
    //批量禁用公司
    public boolean batchDisableCompany(String ids) {
        List<Long> list = new ArrayList<>();
        String[] arr = ids.split(",");
        for(String id : arr) {
            long companyId = Long.valueOf(id);
            if(companyId >= 0) {
                list.add(Long.valueOf(id));
            }
        }
        return companyMapper.batchDisableCompany(list) > 0;
    }
    //批量启用公司
    public boolean batchEnableCompany(String ids) {
        List<Long> list = new ArrayList<>();
        String[] arr = ids.split(",");
        for(String id : arr) {
            long companyId = Long.valueOf(id);
            if(companyId >= 0) {
                list.add(Long.valueOf(id));
            }
        }
        return companyMapper.batchEnableCompany(list) > 0;
    }

}
