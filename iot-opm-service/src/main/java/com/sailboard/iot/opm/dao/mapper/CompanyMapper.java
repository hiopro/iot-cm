package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Company;
import com.sailboard.iot.opm.dao.entity.CompanySpy;
import com.sailboard.iot.opm.dao.entity.PartnerInfo;
import com.sailboard.iot.opm.dao.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface CompanyMapper {
    @Delete({
        "delete from company",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into company (id, name, ",
        "social_code, business_license, ",
        "license_path, status, ",
        "address, leader, ",
        "leader_name, mobile, ",
        "email, region, contract, ",
        "contract_path, type, ",
        "founder, founder_name, ",
        "remarks, sequence_number, ",
        "create_time, update_time，belong_company,bank_card_number,alipay_account,wechat_account,agreement_path,agreement_name)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{socialCode,jdbcType=VARCHAR}, #{businessLicense,jdbcType=VARCHAR}, ",
        "#{licensePath,jdbcType=VARCHAR}, #{status,jdbcType=TINYINT}, ",
        "#{address,jdbcType=VARCHAR}, #{leader,jdbcType=BIGINT}, ",
        "#{leaderName,jdbcType=VARCHAR}, #{mobile,jdbcType=BIGINT}, ",
        "#{email,jdbcType=VARCHAR}, #{region,jdbcType=VARCHAR}, #{contract,jdbcType=BIGINT}, ",
        "#{contractPath,jdbcType=VARCHAR}, #{type,jdbcType=INTEGER}, ",
        "#{founder,jdbcType=BIGINT}, #{founderName,jdbcType=VARCHAR}, ",
        "#{remarks,jdbcType=VARCHAR}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}，#{belongCompany,jdbcType=BIGINT})",
            "#{bankCardNumber,jdbcType=VARCHAR}, #{alipayAccount,jdbcType=VARCHAR},#{wechatAccount,jdbcType=VARCHAR},#{agreementPath,jdbcType=VARCHAR},#{agreementName,jdbcType=VARCHAR})"

    })
    int insert(Company record);

    int insertSelective(Company record);

    @Select({
        "select",
        "id, name, social_code, business_license, license_path, status, address, leader, ",
        "leader_name, mobile, email, region, contract, contract_path, type, founder, ",
        "founder_name, remarks, sequence_number, create_time, update_time, belong_company,",
            "bank_card_number, alipay_account, wechat_account,agreement_path,agreement_name",
        "from company",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Company selectByPrimaryKey(Long id);

    @Select({
        "select",
        "id, name, social_code, business_license, license_path, status, address, leader, ",
        "leader_name, mobile, email, region, contract, contract_path, type, founder, ",
        "founder_name, remarks, sequence_number, create_time, update_time,belong_company",
            "bank_card_number, alipay_account, wechat_account,agreement_path,agreement_name",
        "from company",
        "where name = #{name,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Company selectByAgentName(String name);

    int updateByPrimaryKeySelective(Company record);

    @Update({
        "update company",
        "set name = #{name,jdbcType=VARCHAR},",
          "social_code = #{socialCode,jdbcType=VARCHAR},",
          "business_license = #{businessLicense,jdbcType=VARCHAR},",
          "license_path = #{licensePath,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "address = #{address,jdbcType=VARCHAR},",
          "leader = #{leader,jdbcType=BIGINT},",
          "leader_name = #{leaderName,jdbcType=VARCHAR},",
          "mobile = #{mobile,jdbcType=BIGINT},",
          "email = #{email,jdbcType=VARCHAR},",
          "region = #{region,jdbcType=VARCHAR},",
          "contract = #{contract,jdbcType=BIGINT},",
          "contract_path = #{contractPath,jdbcType=VARCHAR},",
          "type = #{type,jdbcType=INTEGER},",
          "founder = #{founder,jdbcType=BIGINT},",
          "founder_name = #{founderName,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "belong_company = #{belongCompany,jdbcType=BIGINT},",
            "bank_card_number = #{bankCardNumber,jdbcType=VARCHAR},",
            "alipay_account = #{alipayAccount,jdbcType=VARCHAR},",
            "wechat_account = #{wechatAccount,jdbcType=VARCHAR},",
            "agreement_path = #{agreementPath,jdbcType=VARCHAR},",
            "agreement_name = #{agreementName,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Company record);

    /*自己添加 获取公司列表*/
    List<Company> getCompanyList(Map map);

    int batchDelCompany(List<Long> ids);

    @Update({
            "update company",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateCompanyStatus(@Param("id") Long id, @Param("status") Integer status);

    @Update({
            "update company",
            "set agreement_path = null,agreement_name = null",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int clearCompanyAgreement(@Param("id") Long id);//清除该公司的合作协议


    @Select({
            "select",
            "id, name, social_code, business_license, license_path, status, address, leader, ",
            "leader_name, mobile, email, region, contract, contract_path, type, founder, ",
            "founder_name, remarks, sequence_number, create_time, update_time,belong_company",
            "bank_card_number, alipay_account, wechat_account,agreement_path,agreement_name",
            "from company",
            "where social_code = #{socialCode,jdbcType=VARCHAR}"
    })
    @ResultMap("BaseResultMap")
    Company getBySocialCode(String socialCode);

    List<PartnerInfo> getPartnerInfoList(Map map);
    /*    @Select ({
                "SELECT " +
                        "company.id ,company.`name`,company.social_code,company.leader_name,company.mobile,company.create_time ," +
                        "COUNT(partener_id) as totalPartner " +
                        "FROM charging_station,company " +
                        "WHERE charging_station.manager_id = company.id " +
                        "GROUP BY manager_id"
        })
        @ResultMap("BaseResultMap")*/
//列出代理商的总体状况
    List <CompanySpy> getCompany_Agent_Spy_List(Map map);

    //获取代理商的总电站数和总合作伙伴数
    @Select({
            "select COUNT(manager_id) AS totalStation ,COUNT(partener_id) as totalPartner from charging_station " +
                    "where manager_id = #{companyID,jdbcType=BIGINT}"
    })
    CompanySpy getTotal(String companyID);
    //根据ID查询company
    @Select({
            "select * from company where id = #{ID,jdbcType=BIGINT}"
    })
    Company getCompanyByID(String ID);

    //批量禁用公司
    int batchDisableCompany(List<Long> ids);
    //批量启用公司
    int batchEnableCompany(List<Long>ids);

    //根据name查询company
    @Select({
            "select * from company where name = #{name,jdbcType=VARCHAR}"
    })
    Company getCompanyByName(String name);

    @Select({
            "select * from company where social_code = #{socialCode,jdbcType=VARCHAR}"
    })
    Company getCompanyBySocialCode(String socialCode);

    @Select({
            "select * from company where name = #{name,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT}"
    })
    Company getOtherCompanyByName(@Param("name")String name,@Param("id")String id);

    @Select({
            "select * from company where social_code = #{socialCode,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT}"
    })
    Company getOtherCompanyBySocialCode(@Param("socialCode")String socialCode,@Param("id")String id);
}