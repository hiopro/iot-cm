package com.sailboard.iot.opm.config;

import com.sailboard.iot.opm.common.domain.VerifyType;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 验证码配置
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix= "verifyCode")
@ConditionalOnExpression("${verifyCode.enable:false}")
@Component
public class VerifyCodeConfig implements Serializable {
    private String imageCode;
    private String number;
    private int imageLength;
    private int numberLength;
    private boolean isTest;
    private String auditMobile;
    private String auditMobileVerifyCode;
    private String auditMobile1;
    private String auditMobileVerifyCode1;

    public String getAuditMobile1() {
        return auditMobile1;
    }

    public void setAuditMobile1(String auditMobile1) {
        this.auditMobile1 = auditMobile1;
    }

    public String getAuditMobileVerifyCode1() {
        return auditMobileVerifyCode1;
    }

    public void setAuditMobileVerifyCode1(String auditMobileVerifyCode1) {
        this.auditMobileVerifyCode1 = auditMobileVerifyCode1;
    }

    public String getAuditMobileVerifyCode() {
        return auditMobileVerifyCode;
    }

    public void setAuditMobileVerifyCode(String auditMobileVerifyCode) {
        this.auditMobileVerifyCode = auditMobileVerifyCode;
    }

    public String getAuditMobile() {
        return auditMobile;
    }

    public void setAuditMobile(String auditMobile) {
        this.auditMobile = auditMobile;
    }

    public boolean getIsTest() {
        return isTest;
    }

    public void setIsTest(boolean test) {
        isTest = test;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setImageLength(int imageLength) {
        this.imageLength = imageLength;
    }

    public void setNumberLength(int numberLength) {
        this.numberLength = numberLength;
    }

    public String getImageCode() {
        return imageCode;
    }

    public String getNumber() {
        return number;
    }

    public int getImageLength() {
        return imageLength;
    }

    public int getNumberLength() {
        return numberLength;
    }

    public boolean isTest() {
        return isTest;
    }

    public void setTest(boolean test) {
        isTest = test;
    }

    public String generateCode(VerifyType verifyType) {
        StringBuilder code = new StringBuilder();
        if(getIsTest()) {
            return "111111";
        } else {
            switch (verifyType) {
                case IMAGE:
                    for (int i = 0; i < imageLength; i++) {
                        code.append(imageCode.charAt(RandomUtils.nextInt(0, 62)));
                    }
                    break;
                case NUMBER:
                case EMAIL_LINK:
                    for (int i = 0; i < numberLength; i++) {
                        code.append(number.charAt(RandomUtils.nextInt(0, 10)));
                    }
            }
            return code.toString();
        }
    }
}
