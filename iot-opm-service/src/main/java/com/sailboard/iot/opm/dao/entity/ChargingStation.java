package com.sailboard.iot.opm.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ChargingStation {
    private Long id;

    private String name;

    private Byte status;

    private Long sequenceNumber;

    private Date createTime;

    private Date updateTime;

    private String creater;

    private Long createrId;

    private Long managerId;

    private String managerName;

    private Long partenerId;

    private String partenerName;

    private String partenerMobile;

    private Byte operationStarttime;

    private Byte operationEndtime;

    private String address;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private Byte stationType;

    private Long currentUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public Long getPartenerId() {
        return partenerId;
    }

    public void setPartenerId(Long partenerId) {
        this.partenerId = partenerId;
    }

    public String getPartenerName() {
        return partenerName;
    }

    public void setPartenerName(String partenerName) {
        this.partenerName = partenerName;
    }

    public String getPartenerMobile() {
        return partenerMobile;
    }

    public void setPartenerMobile(String partenerMobile) {
        this.partenerMobile = partenerMobile;
    }

    public Byte getOperationStarttime() {
        return operationStarttime;
    }

    public void setOperationStarttime(Byte operationStarttime) {
        this.operationStarttime = operationStarttime;
    }

    public Byte getOperationEndtime() {
        return operationEndtime;
    }

    public void setOperationEndtime(Byte operationEndtime) {
        this.operationEndtime = operationEndtime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public Byte getStationType() {
        return stationType;
    }

    public void setStationType(Byte stationType) {
        this.stationType = stationType;
    }

    public Long getCurrentUser() { return currentUser; }

    public void setCurrentUser(Long currentUser) { this.currentUser = currentUser; }
}