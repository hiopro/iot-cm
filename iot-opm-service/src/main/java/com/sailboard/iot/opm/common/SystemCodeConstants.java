package com.sailboard.iot.opm.common;

public class SystemCodeConstants {
    public static final String STATIC_IMAGE_URL = "opmTest/data/uploader/staticInfoImage";//加载静态图片地址
    public static final String STATIC_UPLOAD_URL ="opmTest/data/uploader";//静态资源上传地址
    public static final String STATIC_FILE_URL ="opmTest/data/uploader/informationFile";//合作协议下载地址
    public static final String OSS_ORBASE_URL = "opmTest/data/uploader/staticInfoImage/qrcodeImage";//oss二维码存放路径
    public static final String STATIC_IMAGE_URL_BD = "/data/uploader/staticInfoImage";//加载静态图片地址
    public static final String STATIC_UPLOAD_URL_BD ="/data/uploader/";//静态资源上传地址
    public static final String STATIC_FILE_URL_BD ="/data/uploader/informationFile";//合作协议下载地址
    public static final String OSS_ORBASE_URL_BD = "/data/uploader/qrcodeImage";//本地暂存二维码路径
//    public static final String STATIC_IMAGE_URL_BD = "d:/uploader/staticInfoImage";//加载静态图片地址
//    public static final String STATIC_UPLOAD_URL_BD ="d:/uploader/";//静态资源上传地址
//    public static final String STATIC_FILE_URL_BD ="d:/uploader/uploader/informationFile";//合作协议下载地址
//    public static final String OSS_ORBASE_URL_BD = "d:/uploader/qrcodeImage";//本地暂存二维码路径
    public static final String   ORBASE_URL="http://localhost:8080/home.html?mac=";//二维码生成路径
    public static final String STATIC_DOWNLOAD_BD = "D:/Download/QRcode";//本地批量下载二维码缓存路径
}
