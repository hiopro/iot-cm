package com.sailboard.iot.opm.dao.entity;

import java.util.Date;

public class PartnerInfo {
    private Long id;

    private String name;

    private Byte status;

    private Long leader;

    private String leaderName;

    private Long mobile;

    private String remarks;

    private Long StationNum;

    private Long founder;

    private Long consoleNum;

    private Long terminalNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getLeader() {
        return leader;
    }

    public void setLeader(Long leader) {
        this.leader = leader;
    }

    public String getLeaderName() {
        return leaderName;
    }

    public void setLeaderName(String leaderName) {
        this.leaderName = leaderName;
    }

    public Long getMobile() {
        return mobile;
    }

    public void setMobile(Long mobile) {
        this.mobile = mobile;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getStationNum() {
        return StationNum;
    }

    public void setStationNum(Long stationNum) {
        StationNum = stationNum;
    }

    public Long getFounder() {
        return founder;
    }

    public void setFounder(Long founder) {
        this.founder = founder;
    }

    public Long getTerminalNum() {
        return terminalNum;
    }

    public void setTerminalNum(Long terminalNum) {
        this.terminalNum = terminalNum;
    }

    public Long getConsoleNum() {
        return consoleNum;
    }

    public void setConsoleNum(Long consoleNum) {
        this.consoleNum = consoleNum;
    }
}
