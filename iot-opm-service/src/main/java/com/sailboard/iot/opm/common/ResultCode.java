
package com.sailboard.iot.opm.common;

import java.io.Serializable;

public class ResultCode implements Serializable {
    //成功
    public static final int SUCCESS_CODE = 200;
    public static final String SUCCESS_MSG = "操作成功";

    //失败
    public static final int ERROR_CODE = 500;
    public static final String ERROR_MSG = "服务器内部错误，请重试";

    public static final int TOKEN_INVALID_CODE = 501;
    public static final String TOKEN_INVALID_MSG = "无效Token，请重新登录";
    public static final String TOKEN_NULL_MSG = "TOKEN 不能为空！";
    public static final String UPDATPWD_NULLID_MSG = "会话超时，请刷新重试！";

    public static final int VERIFY_CODE_ERROR_CODE = 502;
    public static final String VERIFY_CODE_IS_EMPTY_MSG = "验证码不能为空";
    public static final String VERIFY_CODE_OFTEN_MSG = "发送太频繁，请稍后再试";
    public static final String VERIFY_CODE_ERROR_MSG = "验证码错误，请核对重试";
    public static final String VERIFY_CODE_LIMIT_ERROR_MSG = "发送太频繁，超出每日发限";
    public static final String VERIFY_CODE_SEND_ERROR_MSG = "验证码发送失败，请重试";

    /**通用错误码：6xx,记录业务级别通用错误，比如：数据库错误**/
    public static final int PARAM_ERROR_CODE = 601;
    public static final String PARAM_ERROR_MSG = "参数错误，请检查参数";

    public static final int DB_ERROR_CODE = 602;
    public static final String DB_ERROR_MSG = "数据库操作失败，请稍后再试";

    //Token有误
    public static final int TOKEN_ERROR_CODE = 603;
    public static final String TOKEN_ERROR_MSG = "Token过期，请重新登录";


    /**业务相关错误码：7xx,记录业务级别具体错误，比如登陆密码错误**/
    public static final int ERROR_CODE_701 = 701;
    public static final String ERROR_CODE_701_MSG_1 = "该序列号不存在";
    public static final String ERROR_CODE_701_MSG_2 = "该序列号已绑定";
    public static final int ERROR_CODE_702 = 702;
    public static final String ERROR_CODE_702_MSG = "该序列号有多个，请检查序列号";
    public static final int ERROR_CODE_703 = 703;
    public static final String ERROR_CODE_703_MSG_1 = "请启用终端后再绑定";
    public static final String ERROR_CODE_703_MSG_2 = "请启用中控后再绑定";
    public static final int ERROR_CODE_704 = 704;
    public static final String ERROR_CODE_704_MSG = "查不到该代理商！";
    public static final int ERROR_CODE_705 = 705;
    public static final String ERROR_CODE_705_MSG = "该代理商已绑定其他电站！";
    public static final int ERROR_CODE_706 = 706;
    public static final int ERROR_CODE_707 = 707;
    public static final int ERROR_CODE_708 = 708;
    public static final int ERROR_CODE_709 = 709;

}
