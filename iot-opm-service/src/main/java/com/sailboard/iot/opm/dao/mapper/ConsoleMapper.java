package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Console;
import com.sailboard.iot.opm.dao.entity.EquipmentSituation;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ConsoleMapper {
    @Delete({
        "delete from console",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into console (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "serial_number, station_id, ",
        "terminal_number, remarks, ",
        "qr_code, qr_code_url,agent_id,agent_name,bind_status),",
            "charge_method_id, charge_standard_id,machine_code,business_code ",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{serialNumber,jdbcType=VARCHAR}, #{stationId,jdbcType=BIGINT}, ",
        "#{terminalNumber,jdbcType=TINYINT}, #{remarks,jdbcType=VARCHAR}, ",
        "#{qrCode,jdbcType=VARCHAR}, #{qrCodeUrl,jdbcType=VARCHAR},)",
            "#{agentId,jdbcType=BIGINT}, #{agentName,jdbcType=VARCHAR},)",
            "#{partnerId,jdbcType=BIGINT}, #{partnerName,jdbcType=VARCHAR},)",
            "#{bindStatus,jdbcType=TINYINT},#{machineCode,jdbcType=VARCHAR},#{businessCode,jdbcType=VARCHAR})"
    })
    int insert(Console record);

    int insertSelective(Console record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "serial_number, station_id, terminal_number, remarks, qr_code, qr_code_url,agent_id,agent_name,bind_status,partner_id,partner_name,machine_code,business_code",
        "from console",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Console selectByPrimaryKey(Long id);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "serial_number, station_id, terminal_number, remarks, qr_code, qr_code_url,agnet_id,agent_name,bind_status,partner_id,partner_name,machine_code,business_code",
        "from console",
        "where serial_number = #{serialNumber,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    List<Console> selectBySerialNumber(String serialNumber);

    @Select({
           "select * from console where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Console getConsoleById(Long id);

    int updateByPrimaryKeySelective(Console record);

    @Update({
        "update console",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "serial_number = #{serialNumber,jdbcType=VARCHAR},",
          "station_id = #{stationId,jdbcType=BIGINT},",
          "terminal_number = #{terminalNumber,jdbcType=TINYINT},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
          "qr_code = #{qrCode,jdbcType=VARCHAR},",
          "qr_code_url = #{qrCodeUrl,jdbcType=VARCHAR},",
            "agent_id = #{agentId,jdbcType=VARCHAR},",
            "agent_name = #{agentName,jdbcType=VARCHAR},",
            "bind_status = #{bindStatus,jdbcType=VARCHAR},",
            "partner_id = #{partnerId,jdbcType=BIGINT},",
            "partner_name = #{partnerName,jdbcType=VARCHAR},",
            "machine_code = #{machineCode,jdbcType=VARCHAR},",
            "business_code = #{businessCode,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Console record);

    /*自定义*/
    List<Console> getConsoleList(Map map);

    @Update({
            "update console",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateConsoleStatus(@Param("id") Long id, @Param("status") Integer status);

    @Update({
            "update console",
            "set station_id = #{stationId,jdbcType=BIGINT}",
            "where serial_number = #{serialNumber,jdbcType=VARCHAR}"
    })
    int updateBySerialNumber(@Param("stationId") Long stationId, @Param("serialNumber") String serialNumber);

    @Update({
            "update console",
            "set terminal_number = #{terminalNumber,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByTerminalNumber(@Param("id") Long id, @Param("terminalNumber") Integer terminalNumber);

    int batchDelete(List<Long> ids);

   //代理商查看设备情况
    List<EquipmentSituation> getAgentEquipmentSituationList(Map map);
    //平台公司查看设备情况
    List<EquipmentSituation> getCompanyEquipmentSituationList(Map map);
    //合作伙伴公司查看设备情况
    List<EquipmentSituation> getPartnerEquipmentSituationList(Map map);
    //代理商查看中控
    List<Console> getConsoleListByPartnerId(Map map);
    //获得平台公司设置代理商的绑定设备列表
    List<Console>getCompanyAgentConsole(Map map);
    //获得代理商为电站绑定中控列表
    List<Console>getConsoleForStationList(Map map);
    //平台公司解绑中控

    @Update({
            "update console",
            "set agent_id = null ,bind_status = 1,agent_name= null",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int unBindConsole(@Param("id") Long id);


    //电站解除绑定中控
    int unBindStationConsole(Console console);

    Console getConsoleByConsole(Console console);

    //中控去重
    Console getConsoleByMachineCode(Console console);
    Console getConsoleByBusinessCode(Console console);
    int batchDisableConsole(List<Long> ids);//批量禁用Console
    int batchEnableConsole(List<Long> ids);

}