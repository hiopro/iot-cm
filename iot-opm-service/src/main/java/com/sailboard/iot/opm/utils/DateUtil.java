package com.sailboard.iot.opm.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil extends DateTimeUtil {

	/**
	 * 获得两个日期之前相差的天数
	 */
	public static int getDaysInterval(String startStr, String endStr) {
		Date startDate = stringToDate(startStr);
		Date endDate = stringToDate(endStr);
		if (startDate.after(endDate)) {
			Date t = startDate;
			startDate = endDate;
			endDate = t;
		}
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		int days = (int) ((endCalendar.getTimeInMillis() - startCalendar
				.getTimeInMillis()) / (3600L * 1000 * 24));
		return days;
	}

	/**
	 * 获得两个日期之间相差的 type 1:天、2:时、3:分、4秒
	 * 
	 */
	public static int getTimesInterval(String startStr, String endStr, int type) {
		Date startDate = stringToDate(startStr);
		Date endDate = stringToDate(endStr);
		if (startDate.after(endDate)) {
			Date t = startDate;
			startDate = endDate;
			endDate = t;
		}
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		Long intervals = endCalendar.getTimeInMillis()
				- startCalendar.getTimeInMillis();
		int times = 0;
		if (type == 1) {
			times = (int) ((intervals) / (3600L * 1000 * 24));
		} else if (type == 2) {
			times = (int) ((intervals) / (3600L * 1000));
		} else if (type == 3) {
			times = (int) ((intervals) / (60L * 1000));
		} else if (type == 4) {
			times = (int) ((intervals) / (1L * 1000));
		}
		return times;
	}

	/**
	 * 获得两个日期之前相差的月份
	 * 
	 */
	public static int getMonthsInterval(String startStr, String endStr) {
		Date startDate = stringToDate(startStr);
		Date endDate = stringToDate(endStr);
		if (startDate.after(endDate)) {
			Date t = startDate;
			startDate = endDate;
			endDate = t;
		}
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		Calendar temp = Calendar.getInstance();
		temp.setTime(endDate);
		temp.add(Calendar.DATE, 1);

		int year = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int month = endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);

		if ((startCalendar.get(Calendar.DATE) == 1)
				&& (temp.get(Calendar.DATE) == 1)) {
			return year * 12 + month + 1;
		} else if ((startCalendar.get(Calendar.DATE) != 1)
				&& (temp.get(Calendar.DATE) == 1)) {
			return year * 12 + month;
		} else if ((startCalendar.get(Calendar.DATE) == 1)
				&& (temp.get(Calendar.DATE) != 1)) {
			return year * 12 + month;
		} else {
			return (year * 12 + month - 1) < 0 ? 0 : (year * 12 + month);
		}
	}

	/**
	 * 获得两个日期之前相差的月份
	 * 
	 */
	public static int getYearsInterval(String startStr, String endStr) {
		Date startDate = stringToDate(startStr);
		Date endDate = stringToDate(endStr);
		if (startDate.after(endDate)) {
			Date t = startDate;
			startDate = endDate;
			endDate = t;
		}
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(startDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		Calendar temp = Calendar.getInstance();
		temp.setTime(endDate);
		temp.add(Calendar.DATE, 1);

		int year = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int month = endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);
		if (month > 0) {
			year = year + 1;
		}

		return year;
	}

	/**
	 * 计算开始日期days天之后的日期
	 * 
	 * @param startTime
	 *            开始日期
	 * @param days
	 *            天数
	 * @param format
	 *            格式
	 * @author apple
	 * @date 2012-04-24
	 * @return
	 */
	public static String getAddDates(Timestamp startTime, int days,
			String format) {
		if (startTime != null) {
			Timestamp endTime = DateUtil.getDayStart(startTime, days - 1);
			return DateUtil.toDateString(endTime, format);
		} else {
			return "";
		}
	}

	/**
	 * 计算开始日期days天之后的日期
	 * 
	 *            开始日期
	 * @param days
	 *            天数
	 * @param format
	 *            格式
	 * @author apple
	 * @date 2012-04-24
	 * @return
	 */
	public static String getAddDates(String startDate, int days, String format) {
		if (startDate != null) {
			Timestamp startTime = DateUtil.toTimestamp(startDate);
			Timestamp endTime = DateUtil.getDayStart(startTime, days - 1);
			return DateUtil.toDateString(endTime, format);
		} else {
			return "";
		}
	}

	/**
	 * 计算开始日期增加月数后的日期
	 * 
	 *            开始日期
	 * @param month
	 *            月数
	 * @param format
	 *            返回格式
	 * @author apple
	 * @date 2012-04-24
	 * @return
	 */
	public static String getAddMonth(String startDate, int month, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format);
		if (startDate != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(DateUtil.parseDate(startDate, "yyyy-MM-dd"));
			c.add(Calendar.MONTH, month);
			c.add(Calendar.DAY_OF_YEAR, -1);
			Date endDate = c.getTime();
			if(!StringUtils.isBlank(df.format(endDate))){
				return df.format(endDate);
			}
			return "";
		} else {
			return "";
		}
	}
	/**
	 * 计算增加年份后的日期
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String dateAddYear(String str,int ago) {
		 
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		  Date dt;
		try {
			dt = sdf.parse(str);
			 Calendar rightNow = Calendar.getInstance();
			  rightNow.setTime(dt);
			  rightNow.add(Calendar.YEAR, ago);// 日期加1年
			  Date dt1 = rightNow.getTime();
			  String reStr = sdf.format(dt1);
			  return reStr;
		} catch (ParseException e) {
			return "";
		}
		 
		 }

	/**
	 * 返回指定格式的日期
	 * 
	 * @param type
	 *            1:2010-09-02 22; 2:2010-09-22 22:23; 3:2010-09-22 22:23:59
	 * @return
	 */
	public static String getCurrentTime(int type) {
		String format = "yyyy-MM-dd";
		if (type == 1) {
			format = "yyyy-MM-dd HH";
		} else if (type == 2) {
			format = "yyyy-MM-dd HH:mm";
		} else if (type == 3) {
			format = "yyyy-MM-dd HH:mm:ss";
		} else if (type == 4) {
			format = "yyyyMMddHHmmss";
		} else if (type == 5) {
			format = "yyyy-MM-dd'T'HH:mm:ss";
		} else {
			format = "yyyyMMddHHmmsssss";
		}
		return toDateString(DateUtil.nowTimestamp(), format);
	}

	public static String formate(Date date, int type) {
		String format = "yyyy-MM-dd";
		if (type == 1) {
			format = "yyyy-MM-dd HH";
		} else if (type == 2) {
			format = "yyyy-MM-dd HH:mm";
		} else if (type == 3) {
			format = "yyyy-MM-dd HH:mm:ss";
		}else if (type==4){
			format = "yyyyMMddHHmmss";
		}else if(type==5){
			format = "yyyy-MM-dd'T'HH:mm:ss";
		}
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 比较两个日期的大小，如果date1>date2，则返回true，反之返回false
	 * 
	 * @param date1
	 *            第一个日期
	 * @param date2
	 *            第二个日期
	 * @param type
	 *            日期格式
	 * @return
	 */
	public static boolean compareDate(String date1, String date2, int type) {
		boolean result = true;
		String format = "";
		switch (type) {
		case 1:
			format = "yyyy-MM-dd hh";
			break;
		case 2:
			format = "yyyy-MM-dd HH:mm";
			break;
		case 3:
			format = "yyyy-MM-dd HH:mm:ss";
			break;
		case 4:
			format = "yyyy-MM-dd";
		}
		DateFormat df = new SimpleDateFormat(format);
		try {
			if(df!=null){
				Date dt1 = df.parse(date1);
				Date dt2 = df.parse(date2);
				if (dt1.getTime() < dt2.getTime()) {
					result = false;
				}
			}
		} catch (ParseException e) {
		}
		return result;
	}

	/**
	 * @Title: getTimeByMinute
	 * @Description: 获取当前时间minute分钟之前或者之后的时间(这里用一句话描述这个方法的作用)
	 * @param @param minute
	 * @param @return
	 * @return String 返回类型
	 * @throws
	 */
	public static String getTimeByMinute(int minute) {

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.MINUTE, minute);

		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar
				.getTime());
	}
	/**
	 * @Title: getTimeByDay
	 * @Description: 获取当前时间day天之前或者之后的时间(这里用一句话描述这个方法的作用)
	 * @param @param day
	 * @return String 返回类型
	 * @throws
	 */
	public static String getTimeByDay(int day) {

		Calendar calendar = Calendar.getInstance();

		calendar.add(Calendar.DATE, day);

		return new SimpleDateFormat("yyyy-MM-dd").format(calendar
				.getTime());
	}

	/**
	 * 返回时间戳
	 * 
	 * @return
	 */

	public static long getTime() {
		return new Date().getTime();
	}
	/** 
	* @Title: getTimeAgo 
	* @Description: 获取调用时间，毫秒差(这里用一句话描述这个方法的作用) 
	* @param @param starTime 开始时间戳
	* @param @param endTime  结束时间戳
	* @param @return    
	* @return String    返回类型 
	* @throws 
	*/
	public static String getTimeAgo(long startTime,long endTime){
		String result="";
	    if(startTime>0&&endTime>0){
	    	long timeAgo=endTime-startTime;
	    	result=timeAgo+"";
	    }
	    return result;
	}
	
	/**
	 * 获取年龄
	 * @param startDate
	 * @return
	 */
	  
	public static Integer getAgeByBirthday(String startDate) {
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		Date birthday = null;
		try {
			birthday = myFormatter.parse(startDate);
		} catch (ParseException e) {
		}
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		if (cal.before(birthday)) {
			throw new IllegalArgumentException(
					"The birthDay is before Now.It's unbelievable!");
		}

		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH) + 1;
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);

		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(birthday);
		int yearBirth = cal1.get(Calendar.YEAR);
		int monthBirth = cal1.get(Calendar.MONTH) + 1;
		int dayOfMonthBirth = cal1.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;

		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth 
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth 
				age--;
			}
		}
		return age;
	}
	

	/**
	 * 测试方法<br>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
//		Calendar calendar = Calendar.getInstance();
//
//		calendar.add(Calendar.DATE, -7);
//
//	  String date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar
//				.getTime());
		// getMonthsInterval("2008-05-07", "2009-03-31");
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// long time=Long.parseLong("1410537600")+86399;
		// System.out.println(df.format(time*1000));
		// long time1=Long.parseLong("1410507977");
		// System.out.println(df.format(time1*1000));
		// System.out.println( DateUtil.getCurrentTime(4));
		// System.out.println(getDaysInterval(endDate,startDate));

//		List<Integer> aa = new ArrayList<Integer>();
//		for (int i = 1; i <= 30; i++) {
//			aa.add(i);
//		}
//		List<Integer> bb = new ArrayList<Integer>();
//		for (int i = 1; i <= aa.size(); i++) {
//			System.out.println(i + "=" + i % 3);
//			if (i % 3 != 0) {
//				bb.add(aa.get(i - 1));
//			}
//		}
	//	System.out.println(date);
		
		//	String year = getAge("1991-04-21","2017-04-22");
//		int year = getAgeByBirthday("1991-06-29");
//			System.out.println(year);
		
		//String a = dateAddYear("1991-04-21 ",1);
		//File df=new File("D:\\workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\tmonline_branch_DEV_udp-web\\WEB-INF\\classes\\META-INF\\app_config\\key\\META-INF\\app_config\\key\\pubBPMOLExp.dat");
	//	System.out.println(df.getName());
		
		// date=DateUtil.stringToDate(DateUtil.getCurrentTime(3));
		long ss=getTime();
		long s2=getTime();
		long s3=getTime();
		System.out.println(getTimeAgo(ss,getTime()));
	}

}
