package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Rate;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface RateMapper {
    @Delete({
        "delete from rate",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into rate (id, status, ",
        "create_time, update_time, ",
        "creater_id, creater, ",
        "remarks, platform_rate, ",
        "service_guarantee, internet_account, ",
        "company_id, company_name, ",
        "agent_id, agent_name, ",
        "partner_id, partner_name)",
        "values (#{id,jdbcType=BIGINT}, #{status,jdbcType=TINYINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{createrId,jdbcType=BIGINT}, #{creater,jdbcType=VARCHAR}, ",
        "#{remarks,jdbcType=VARCHAR}, #{platformRate,jdbcType=DECIMAL}, ",
        "#{serviceGuarantee,jdbcType=DECIMAL}, #{internetAccount,jdbcType=DECIMAL}, ",
            "#{profitRule,jdbcType=DECIMAL}, #{type,jdbcType=TINYINT}, ",
        "#{companyId,jdbcType=BIGINT}, #{companyName,jdbcType=VARCHAR}, ",
        "#{agentId,jdbcType=BIGINT}, #{agentName,jdbcType=VARCHAR}, ",
        "#{partnerId,jdbcType=BIGINT}, #{partnerName,jdbcType=VARCHAR})"
    })
    int insert(Rate record);

    int insertSelective(Rate record);

    @Select({
        "select",
        "id, status, create_time, update_time, creater_id, creater, remarks, platform_rate, ",
        "service_guarantee, internet_account, company_id, company_name, agent_id, agent_name, ",
        "partner_id, partner_name,profit_rule,type",
        "from rate",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Rate selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Rate record);

    @Update({
        "update rate",
        "set status = #{status,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR},",
          "platform_rate = #{platformRate,jdbcType=DECIMAL},",
          "service_guarantee = #{serviceGuarantee,jdbcType=DECIMAL},",
          "internet_account = #{internetAccount,jdbcType=DECIMAL},",
            "profit_rule = #{profitRule,jdbcType=DECIMAL},",
            "type = #{type,jdbcType=TINYINT},",
          "company_id = #{companyId,jdbcType=BIGINT},",
          "company_name = #{companyName,jdbcType=VARCHAR},",
          "agent_id = #{agentId,jdbcType=BIGINT},",
          "agent_name = #{agentName,jdbcType=VARCHAR},",
          "partner_id = #{partnerId,jdbcType=BIGINT},",
          "partner_name = #{partnerName,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Rate record);

    @Select({
            "select",
            "id, status, create_time, update_time, creater_id, creater, remarks, platform_rate, ",
            "service_guarantee, internet_account, company_id, company_name, agent_id, agent_name, ",
            "partner_id, partner_name,profit_rule,type",
            "from rate",
            "where agent_id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Rate selectByAgentID(Long id);//通过代理商的ID获得记录


    @Select({
            "select",
            "id, status, create_time, update_time, creater_id, creater, remarks, platform_rate, ",
            "service_guarantee, internet_account, company_id, company_name, agent_id, agent_name, ",
            "partner_id, partner_name,profit_rule,type",
            "from rate",
            "where partner_id = #{partnerId,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Rate getRateByPartnerId(Long id);//通过合作伙伴的ID获得记录，type设置为2（来取收益规则）

}