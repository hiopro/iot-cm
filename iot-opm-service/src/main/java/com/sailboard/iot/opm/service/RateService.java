package com.sailboard.iot.opm.service;

import com.sailboard.iot.opm.dao.entity.Rate;

public interface RateService {
    /**
     * 平台公司获得当前代理商的费率设置
     * @param companyID
     * @return
     */
    Rate getRateByCompanyID(Long companyID);
    boolean  updateRateSelective(Rate rate);
    boolean  insertRateSelective(Rate rate);

    Rate getRateByPartnerID(Long partnerId);
}
