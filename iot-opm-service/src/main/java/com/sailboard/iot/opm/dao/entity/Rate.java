package com.sailboard.iot.opm.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Rate {
    private Long id;

    private Byte status;

    private Date createTime;

    private Date updateTime;

    private Long createrId;

    private String creater;

    private String remarks;

    private BigDecimal platformRate;

    private BigDecimal serviceGuarantee;

    private BigDecimal internetAccount;

    private Byte type;

    private BigDecimal profitRule;

    private Long companyId;

    private String companyName;

    private Long agentId;

    private String agentName;

    private Long partnerId;

    private String partnerName;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getPlatformRate() {
        return platformRate;
    }

    public void setPlatformRate(BigDecimal platformRate) {
        this.platformRate = platformRate;
    }

    public BigDecimal getServiceGuarantee() {
        return serviceGuarantee;
    }

    public void setServiceGuarantee(BigDecimal serviceGuarantee) {
        this.serviceGuarantee = serviceGuarantee;
    }

    public BigDecimal getInternetAccount() {
        return internetAccount;
    }

    public void setInternetAccount(BigDecimal internetAccount) {
        this.internetAccount = internetAccount;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public BigDecimal getProfitRule() {
        return profitRule;
    }

    public void setProfitRule(BigDecimal profitRule) {
        this.profitRule = profitRule;
    }
}