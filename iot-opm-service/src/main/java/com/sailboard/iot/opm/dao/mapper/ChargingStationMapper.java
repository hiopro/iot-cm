package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ChargingStation;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ChargingStationMapper {
    @Delete({
        "delete from charging_station",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into charging_station (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "manager_id, manager_name, ",
        "partener_id, partener_name, ",
        "partener_mobile, operation_starttime, ",
        "operation_endtime, address, ",
        "longitude, latitude, ",
        "station_type)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{managerId,jdbcType=BIGINT}, #{managerName,jdbcType=VARCHAR}, ",
        "#{partenerId,jdbcType=BIGINT}, #{partenerName,jdbcType=VARCHAR}, ",
        "#{partenerMobile,jdbcType=VARCHAR}, #{operationStarttime,jdbcType=TINYINT}, ",
        "#{operationEndtime,jdbcType=TINYINT}, #{address,jdbcType=VARCHAR}, ",
        "#{longitude,jdbcType=DECIMAL}, #{latitude,jdbcType=DECIMAL}, ",
        "#{stationType,jdbcType=TINYINT})"
    })
    int insert(ChargingStation record);

    int insertSelective(ChargingStation record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "manager_id, manager_name, partener_id, partener_name, partener_mobile, operation_starttime, ",
        "operation_endtime, address, longitude, latitude, station_type",
        "from charging_station",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ChargingStation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChargingStation record);

    @Update({
        "update charging_station",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "manager_id = #{managerId,jdbcType=BIGINT},",
          "manager_name = #{managerName,jdbcType=VARCHAR},",
          "partener_id = #{partenerId,jdbcType=BIGINT},",
          "partener_name = #{partenerName,jdbcType=VARCHAR},",
          "partener_mobile = #{partenerMobile,jdbcType=VARCHAR},",
          "operation_starttime = #{operationStarttime,jdbcType=TINYINT},",
          "operation_endtime = #{operationEndtime,jdbcType=TINYINT},",
          "address = #{address,jdbcType=VARCHAR},",
          "longitude = #{longitude,jdbcType=DECIMAL},",
          "latitude = #{latitude,jdbcType=DECIMAL},",
          "station_type = #{stationType,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ChargingStation record);

    /*自己增加*/
    //获取电站列表
    List<ChargingStation> getChargingStationList(Map map);

    //批量删除充电站
    int batchDelete(List<Long> ids);

    //更新电站状态
    @Update({
            "update charging_station",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStationStatus(@Param("id") Long id, @Param("status") Integer status);

    //更新代理商ID和名称
    @Update({
            "update charging_station",
            "set manager_id = #{managerId,jdbcType=TINYINT},",
            "manager_name = #{managerName,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStationAgent(@Param("id")Long id, @Param("managerId") Long managerId, @Param("managerName") String managerName);

}