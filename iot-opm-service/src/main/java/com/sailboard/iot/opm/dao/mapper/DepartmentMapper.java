package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.Department;
import com.sailboard.iot.opm.dao.entity.DepartmentUser;
import com.sailboard.iot.opm.dao.entity.Role;
import com.sailboard.iot.opm.dao.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface DepartmentMapper {
    @Delete({
        "delete from department",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into department (id, name, ",
        "company_id, status, ",
        "sequence_number, create_time, ",
        "update_time)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{companyId,jdbcType=VARCHAR}, #{status,jdbcType=TINYINT}, ",
        "#{sequenceNumber,jdbcType=BIGINT}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP},#{remarks,jdbcType=VARCHAR},)"
    })
    int insert(Department record);

    int insertSelective(Department record);

    @Select({
        "select",
        "id, name, company_id, status, sequence_number, create_time, update_time, remarks",
        "from department",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    Department selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Department record);

    @Update({
        "update department",
        "set name = #{name,jdbcType=VARCHAR},",
          "company_id = #{companyId,jdbcType=VARCHAR},",
            "remarks = #{remarks,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(Department record);

    List<Department> getDepartmentList(Map map);

    int batchDeleteDepartment(List<Long> ids);

    @Update({
            "update department",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateDepartmentStatus(@Param("id") Long id, @Param("status") Integer status);

    @Select({
            "select department.* from department, department_user",
            "where department.id = department_user.department_id",
            "and user_id = #{userId,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    List<Department> getUserDepartment(Long userId);
    //名称去重增加和编辑
    @Select({
            "select * from department where name = #{name,jdbcType=VARCHAR} and company_id = #{companyId,jdbcType=BIGINT}"
    })
    Department getDepartmentByName(@Param("name")String name,@Param("companyId")Long companyId);

    @Select({
            "select * from department where name = #{name,jdbcType=VARCHAR} and id <> #{id,jdbcType=BIGINT} and company_id = #{companyId,jdbcType=BIGINT}"
    })
    Department getOtherDepartmentByName(@Param("name")String name,@Param("id")Long id,@Param("companyId")Long companyId);

}