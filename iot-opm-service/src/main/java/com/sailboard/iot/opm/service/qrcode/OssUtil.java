package com.sailboard.iot.opm.service.qrcode;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.PutObjectRequest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

public class OssUtil {

	private static String endpoint = "oss-cn-beijing.aliyuncs.com";
	private static String accessKeyId = "LTAI3zsFLe7kcDIP";
	private static String accessKeySecret =  "5YKln5mFKKHJcSfi8891yPKnQ9lnvJ";
	private static String bucketName =  "hbwl-static";
	private static OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
	/**
	 * 上传文件_本地文件形式
	 * @param file
	 * @param fileName
	 * @return
	 */
	public static String uploadToOss(File file,String fileName){
		String result="ok";
		try {
			System.out.println("Uploading a new object to OSS from a file\n");
			client.putObject(new PutObjectRequest(bucketName, fileName,file));

		} catch (OSSException oe) {
			 System.out.println("Caught an OSSException, which means your request made it to OSS, "
			 + "but was rejected with an error response for some reason.");
			 System.out.println("Error Message: " + oe.getErrorCode());
			 System.out.println("Error Code:       " + oe.getErrorCode());
			 System.out.println("Request ID:      " + oe.getRequestId());
			 System.out.println("Host ID:           " + oe.getHostId());
			 result=oe.getErrorCode();
		} catch (ClientException ce) {
			 System.out.println("Caught an ClientException, which means the client encountered "
			 +
			 "a serious internal problem while trying to communicate with OSS, "
			 + "such as not being able to access the network.");
			 System.out.println("Error Message: " + ce.getMessage());
			 result=ce.getErrorCode();
		} finally {
			/*
			 * Do not forget to shut down the client finally to release all
			 * allocated resources.
			 */
//			client.shutdown();
		}
		return result;
	}
	
	/**
	 * 上传文件_流形式
	 * @param fileName
	 * @return
	 */
	public static String uploadToOss(InputStream inputStream,String fileName){
		String result="ok";
		try {
			System.out.println("Uploading a new object to OSS from a file\n");
			client.putObject(new PutObjectRequest(bucketName, fileName,inputStream));

		} catch (OSSException oe) {
			 System.out.println("Caught an OSSException, which means your request made it to OSS, "
			 + "but was rejected with an error response for some reason.");
			 System.out.println("Error Message: " + oe.getErrorCode());
			 System.out.println("Error Code:       " + oe.getErrorCode());
			 System.out.println("Request ID:      " + oe.getRequestId());
			 System.out.println("Host ID:           " + oe.getHostId());
			 result=oe.getErrorCode();
		} catch (ClientException ce) {
			 System.out.println("Caught an ClientException, which means the client encountered "
			 +
			 "a serious internal problem while trying to communicate with OSS, "
			 + "such as not being able to access the network.");
			 System.out.println("Error Message: " + ce.getMessage());
			 result=ce.getErrorCode();
		} finally {
			/*
			 * Do not forget to shut down the client finally to release all
			 * allocated resources.
			 */
//			client.shutdown();
		}
		return result;
	}
	
	/**
	 * 创建目录
	 * @param folderName
	 * @return
	 */
	public static String createFolderToOss(String folderName){
		String result="ok";
		try {
			System.out.println("Uploading a new object to OSS from a file\n");
			client.putObject(bucketName, folderName,new ByteArrayInputStream(new byte[0]));
		} catch (OSSException oe) {
			 System.out.println("Caught an OSSException, which means your request made it to OSS, "
			 + "but was rejected with an error response for some reason.");
			 System.out.println("Error Message: " + oe.getErrorCode());
			 System.out.println("Error Code:       " + oe.getErrorCode());
			 System.out.println("Request ID:      " + oe.getRequestId());
			 System.out.println("Host ID:           " + oe.getHostId());
			 result=oe.getErrorCode();
		} catch (ClientException ce) {
			 System.out.println("Caught an ClientException, which means the client encountered "
			 +
			 "a serious internal problem while trying to communicate with OSS, "
			 + "such as not being able to access the network.");
			 System.out.println("Error Message: " + ce.getMessage());
			 result=ce.getErrorCode();
		} finally {
			/*
			 * Do not forget to shut down the client finally to release all
			 * allocated resources.
			 */
//			client.shutdown();
		}
		return result; 
	}
	
	
	/**
	 * 获取文件
	 * @param fileName
	 * @return
	 */
	public static InputStream getFileFromOss(String fileName){
		System.out.println("getfilefromoss---"+fileName);
        OSSObject object = client.getObject(new GetObjectRequest(bucketName, fileName));
        InputStream in=null;
        if(object!=null){
        	in= object.getObjectContent();
        	
        }
         return in;
	}
	
	/**
	 * 判断文件是否存在
	 * @param fileName
	 * @return
	 */
	public static boolean isFileExist(String fileName){
		return client.doesObjectExist(bucketName, fileName);
	}
	
	/**
	 * 删除文件
	 * @param fileName
	 */
	public static void deleteFile(String fileName){
		client.deleteObject(bucketName, fileName);
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file=new File("d://AccurateMarketing.log");
		System.out.println(file.getPath());
	}
}
