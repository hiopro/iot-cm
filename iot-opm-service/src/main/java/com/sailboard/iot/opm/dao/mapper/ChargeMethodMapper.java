package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ChargeMethod;
import com.sailboard.iot.opm.dao.entity.ChargeStandard;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ChargeMethodMapper {
    @Delete({
        "delete from charge_method",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into charge_method (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "charge_time, console_id, ",
        "remarks)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{chargeTime,jdbcType=DECIMAL}, #{consoleId,jdbcType=BIGINT}, ",
        "#{remarks,jdbcType=VARCHAR})"
    })
    int insert(ChargeMethod record);

    int insertSelective(ChargeMethod record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "charge_time, console_id, remarks",
        "from charge_method",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ChargeMethod selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChargeMethod record);

    @Update({
        "update charge_method",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "charge_time = #{chargeTime,jdbcType=DECIMAL},",
          "console_id = #{consoleId,jdbcType=BIGINT},",
          "remarks = #{remarks,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ChargeMethod record);

    /*自定义*/
    List<ChargeMethod> getList(Map map);

    @Update({
            "update charge_method",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDelete(List<Long> ids);
}