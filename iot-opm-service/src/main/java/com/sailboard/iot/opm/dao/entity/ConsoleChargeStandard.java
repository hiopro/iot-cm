package com.sailboard.iot.opm.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

public class ConsoleChargeStandard {
    private Long id;

    private String name;

    private Byte status;

    private Long sequenceNumber;

    private Date createTime;

    private Date updateTime;

    private String creater;

    private Long createrId;

    private Long powerLower;

    private Long powerUpper;

    private BigDecimal unitRice;

    private Byte unitType;

    private Long terminalId;

    private Byte gearSequence;

    private Long consoleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Long getPowerLower() {
        return powerLower;
    }

    public void setPowerLower(Long powerLower) {
        this.powerLower = powerLower;
    }

    public Long getPowerUpper() {
        return powerUpper;
    }

    public void setPowerUpper(Long powerUpper) {
        this.powerUpper = powerUpper;
    }

    public BigDecimal getUnitRice() {
        return unitRice;
    }

    public void setUnitRice(BigDecimal unitRice) {
        this.unitRice = unitRice;
    }

    public Byte getUnitType() {
        return unitType;
    }

    public void setUnitType(Byte unitType) {
        this.unitType = unitType;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public Byte getGearSequence() {
        return gearSequence;
    }

    public void setGearSequence(Byte gearSequence) {
        this.gearSequence = gearSequence;
    }

    public Long getConsoleId() {
        return consoleId;
    }

    public void setConsoleId(Long consoleId) {
        this.consoleId = consoleId;
    }
}