package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.CompanyRole;
import com.sailboard.iot.opm.dao.entity.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CompanyRoleMapper {
    @Delete({
        "delete from company_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into company_role (id, company_id, ",
        "role_id)",
        "values (#{id,jdbcType=BIGINT}, #{companyId,jdbcType=BIGINT}, ",
        "#{roleId,jdbcType=BIGINT})"
    })
    int insert(CompanyRole record);

    int insertSelective(CompanyRole record);

    @Select({
        "select",
        "id, company_id, role_id",
        "from company_role",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    CompanyRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CompanyRole record);

    @Update({
        "update company_role",
        "set company_id = #{companyId,jdbcType=BIGINT},",
          "role_id = #{roleId,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(CompanyRole record);

    @Delete({
            "delete from company_role",
            "where company_id = #{companyId,jdbcType=BIGINT}"
    })
    int deleteByCompanyId(Long companyId);

    int batchInsert(List<CompanyRole> list);

}