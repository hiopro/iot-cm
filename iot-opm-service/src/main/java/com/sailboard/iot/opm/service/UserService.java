package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.User;
import com.sailboard.iot.opm.common.JsonResult;

/**
 * Created by tiansj on 15/6/23.
 */
public interface UserService {

    boolean addUser(User user);

    boolean deleteUser(Long userId);

    boolean updateUser(User user);

    boolean updateUserStatus(Long id, Integer status);

    User getUser(Long userId);

    User getByUsername(String username);

    PageInfo getUserList(int pageNum, int pageSize, User user);

    boolean batchDelUser(String ids);

    JsonResult bindRole(Long userId, String roleIds);

    JsonResult batchBindRole(String userIds, String roleIds);

    boolean batchDisableUser(String ids);//批量禁用user，leader随着公司禁用而禁用。
    boolean batchEnableUser(String ids);
    //新增去重手机号
    User getUserByMobile(String mobile);
    //编辑去重
    User getOtherUserByName(String name,Long id);//用户名被取消了。
    User getOtherUserByMobile(String mobile,Long id);
    //登陆判断用户名/手机号，密码
    User getUserByUserNameOrMobile(String userName,String mobile);
}
