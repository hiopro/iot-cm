package com.sailboard.iot.opm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.RechargeRecord;
import com.sailboard.iot.opm.dao.mapper.RechargeRecordMapper;
import com.sailboard.iot.opm.service.RechargeRecordService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RechargeRecordServiceImpl implements RechargeRecordService {
    @Resource RechargeRecordMapper rechargeRecordMapper;
    @Override
    public PageInfo getRecharge_RecordList(int pageNum, int pageSize, RechargeRecord rechargeRecord) {
        PageHelper.startPage(pageNum,pageSize);
        List<RechargeRecord> list = rechargeRecordMapper.listRechargeRecord(BeanMapUtils.toMap(rechargeRecord));
        return new PageInfo(list);
    }
}
