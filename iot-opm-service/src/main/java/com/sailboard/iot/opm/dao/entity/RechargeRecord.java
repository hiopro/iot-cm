package com.sailboard.iot.opm.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

public class RechargeRecord {
    private Long id;

    private String name;

    private Byte status;

    private Long sequenceNumber;

    private Date createTime;

    private Date updateTime;

    private String creater;

    private Long createrId;

    private BigDecimal amount;

    private Long accountId;

    private Byte rechargeMethod;

    private Long rechargeCardId;

    private Long userId;

    private String userName;

    private String remarks;

    private byte type ;//消费类型（充值或转账）

    private  String account_type;//消费账户类型4种类型

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Byte getRechargeMethod() {
        return rechargeMethod;
    }

    public void setRechargeMethod(Byte rechargeMethod) {
        this.rechargeMethod = rechargeMethod;
    }

    public Long getRechargeCardId() {
        return rechargeCardId;
    }

    public void setRechargeCardId(Long rechargeCardId) {
        this.rechargeCardId = rechargeCardId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}