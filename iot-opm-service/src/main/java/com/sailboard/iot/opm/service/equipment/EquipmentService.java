package com.sailboard.iot.opm.service.equipment;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.dao.entity.ChargingStation;
import com.sailboard.iot.opm.dao.entity.Console;
import com.sailboard.iot.opm.dao.entity.EquipmentSituation;
import com.sailboard.iot.opm.dao.entity.Terminal;

import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.service.equipment
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/25 20:44
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/25 20:44
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
public interface EquipmentService {
    PageInfo getTerminalList(int pageNum, int pageSize, Terminal terminal);
    boolean addTerminal(Terminal terminal);
    boolean deleteTerminal(Long terminalId);
    boolean updateTerminal(Terminal terminal);
    boolean updateTerminalStatus(Long id,Integer status);
    boolean batchDelTerminal(String ids);
    PageInfo getConsoleList(int pageNum, int pageSize, Console console);
    boolean addConsole(Console console);
    boolean deleteConsole(Long consoleId);
    boolean updateConsole(Console console);
    boolean updateConsoleStatus(Long id,Integer status);
    boolean batchDelConsole(String ids);
    JsonResult bindingTerminal(Long id, String serialNumber);
    JsonResult bindingConsole(Long id, String serialNumber);
    JsonResult bindingAgent(Long id, String name);
    JsonResult batchAddQrCode(String ids);
    JsonResult batchAddTerminalQrCode(String ids);

    PageInfo getChargingStationList(int pageNum, int pageSize, ChargingStation chargingStation);
    boolean addChargingStation(ChargingStation chargingStation);
    boolean deleteChargingStation(Long chargingStationId);
    boolean updateChargingStation(ChargingStation chargingStation);
    boolean updateChargingStationStatus(Long id,Integer status);
    boolean batchDelChargingStation(String ids);

    PageInfo getAgentEquipmentSituationList(int pageNum, int pageSize, EquipmentSituation equipmentSituation);
     PageInfo getCompanyEquipmentSituationList(int pageNum, int pageSize, EquipmentSituation equipmentSituation);
    PageInfo getPartnerEquipmentSituationList(int pageNum, int pageSize, EquipmentSituation equipmentSituation);


    PageInfo getTerminalListByPartnerId(int pageNum, int pageSize, Terminal terminal);
    PageInfo getConsoleListByPartnerId(int pageNum, int pageSize, Console console);

    PageInfo getCompanyAgentConsoleList(int pageNum,int pageSize,Console console);
    //电站绑定中控，展示
    PageInfo getConsoleForStationList(int pageNum,int pageSize,Console console);

    boolean unBindConsole(Console console);

    //解除电站绑定的中控（修改中控的stationId为空，bindStatus为2）
    boolean unBindStationConsole(Console console);

    ChargingStation getChargingStationById(Long stationId);

    //通过console的部分信息，获取console的全部信息。主要获取id。
    Console getConsoleByConsole(Console console);

    //通过机器码，业务码获取中控，排重。编辑排重时id不为空。
    Console getConsoleByMachineCode(Console console);
    Console getConsoleByBusinessCode(Console console);
    boolean batchDisableConsole(String ids);
    boolean batchEnableConsole(String ids);//批量启用Console

    //通过机器码，业务码获取终端，排重。编辑排重时id不为空。
    Terminal getTerminalByMachineCode(Terminal terminal);
    Terminal getTerminalByBusinessCode(Terminal terminal);
    //批量禁用、启用终端
    boolean batchDisableTerminal(String ids);
    boolean batchEnableTerminal(String ids);

    Console getConsoleById(Long id);
}
