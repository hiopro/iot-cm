
package com.sailboard.iot.opm.common;

/**
 * Created by tiansj on 15/7/16.
 */
public class Constants {

    // shiro redis cache prefix
    public static String SHIRO_REDIS_SESSION = "shiro_redis_session:";
    public static String SHIRO_REDIS_CACHE = "shiro_redis_cache:";
    public static String SESSSION_USER = "SESSION_USER";

    public static final int USER_STATUS_ENABLE = 1;
    public static final int USER_STATUS_DISABLE = 2;

    public static final int ROLE_STATUS_ENABLE = 1;
    public static final int ROLE_STATUS_DISABLE = 2;

    public static final int COMPANY_STATUS_ENABLE = 1;
    public static final int COMPANY_STATUS_DISABLE = 2;

    public static class User {

        public static final String DEFAULT_AVATAR_URL = "http://vii-img.oss-cn-beijing.aliyuncs.com/default_avatar.png";

        public static final int SEX_MEN = 1;
        public static final int SEX_WOMEN = 2;

        public static final int STATUS_ACTIVE = 1;
        public static final int STATUS_LOCKED = 2;

        public static final int INFO_STATUS_INT = 1;
        public static final int INFO_STATUS_DONE = 2;

        public static final int ONLINE_STATUS_NO = 0;
        public static final int ONLINE_STATUS_BUSY = 1;
        public static final int ONLINE_STATUS_YES = 9;

        public static final int THIRD_TYPE_WEIXIN = 1;
        public static final int THIRD_TYPE_QQ = 2;
        public static final int THIRD_TYPE_WEIBO = 3;

        public static final String COUNTRY_CHINA = "CN";


        public static final int AVATAR_STATUS_INIT = 1;
        public static final int AVATAR_STATUS_PASS = 2;
        public static final int AVATAR_STATUS_REJECT = 3;
    }

    public static class Sms {
        public static final int TYPE_MOBILE = 1;
        public static final int TYPE_WITHDRAW = 2;
        public static final int TYPE_SYSTEM = 3;
    }

    public static class Order {
        // 1已创建，2已支付，3已发货，4已取消
        public static final int ORDER_STATUS_INIT = 1;
        public static final int ORDER_STATUS_PAY_SUCCESS = 2;
        public static final int ORDER_STATUS_DELIVER_SUCCESS = 3;
        public static final int ORDER_STATUS_PAY_CANCEL = 4;
        public static final int ORDER_STATUS_PAY_FAILED = 5;

        // 支付类型：1微信，2支付宝，3苹果支付
        public static final int PAY_TYPE_WEIXIN = 1;
        public static final int PAY_TYPE_ALIPAY = 2;
        public static final int PAY_TYPE_APPLE = 3;

        // 支付状态：1待支付，2已支付
        public static final int PAY_STATUS_AWAITING = 1;
        public static final int PAY_STATUS_SUCCESS = 2;

        // 发货状态：1待发货，2已发货
        public static final int DELIVER_STATUS_AWAITING = 1;
        public static final int DELIVER_STATUS_SUCCESS = 2;
    }
}
