package com.sailboard.iot.opm.service.equipment.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.*;
import com.sailboard.iot.opm.dao.mapper.*;
import com.sailboard.iot.opm.service.equipment.EquipmentConfigService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: iot-opm
 * @Package: com.sailboard.iot.opm.service.equipment.impl
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: 候杰
 * @CreateDate: 2017/12/25 20:45
 * @UpdateUser: Neil.Zhou
 * @UpdateDate: 2017/12/25 20:45
 * @UpdateRemark: The modified content
 * @Version: 1.0
 * Copyright: Copyright (c) 2017
 */
@Component
public class EquipmentConfigServiceImpl implements EquipmentConfigService {

    @Resource
    RechargeCardMapper rechargeCardMapper;
    @Resource
    ChargeMethodMapper chargeMethodMapper;
    @Resource
    ChargeStandardMapper chargeStandardMapper;
    @Resource
    ConsoleChargeStandardMapper consoleChargeStandardMapper;

    @Override
    public PageInfo getRechargeCardList(int pageNum, int pageSize, RechargeCard rechargeCard){
        PageHelper.startPage(pageNum, pageSize);
        List<RechargeCard> list = rechargeCardMapper.getList(BeanMapUtils.toMap(rechargeCard));
        return new PageInfo(list);
    }
    @Override
    public boolean addRechargeCard(RechargeCard rechargeCard){
        return rechargeCardMapper.insertSelective(rechargeCard)==1;
    }
    @Override
    public boolean deleteRechargeCard(Long rechargeCardId){
        return rechargeCardMapper.deleteByPrimaryKey(rechargeCardId)==1;
    }

    @Override
    public boolean batchDelRechargeCard(String ids){
        String[] arr = ids.split(",");
        List<Long> delList = new ArrayList<>();
        List<String> cannotDelList = new ArrayList<>();
        for(String id : arr) {
            long roleId = Long.valueOf(id);
            delList.add(Long.valueOf(id));
        }
        int row = rechargeCardMapper.batchDelete(delList);
        return row > 0;
    }

    @Override
    public boolean updateRechargeCard(RechargeCard rechargeCard){
        return rechargeCardMapper.updateByPrimaryKeySelective(rechargeCard)==1;
    }
    @Override
    public boolean updateRechargeCardStatus(Long id,Integer status){
        return rechargeCardMapper.updateStatus(id, status) == 1;
    }


    @Override
    public PageInfo getChargeStandardList(int pageNum, int pageSize, ChargeStandard chargeStandard){
        PageHelper.startPage(pageNum, pageSize);
        List<ChargeStandard> list = chargeStandardMapper.getList(BeanMapUtils.toMap(chargeStandard));
        return new PageInfo(list);
    }
    @Override
    public boolean addChargeStandard(ChargeStandard chargeStandard){
        return chargeStandardMapper.insertSelective(chargeStandard)==1;
    }
    @Override
    public boolean deleteChargeStandard(Long chargeStandardId){
        return chargeStandardMapper.deleteByPrimaryKey(chargeStandardId)==1;
    }
    @Override
    public boolean updateChargeStandard(ChargeStandard chargeStandard){
        return chargeStandardMapper.updateByPrimaryKeySelective(chargeStandard)==1;
    }
    @Override
    public boolean updateChargeStandardStatus(Long id, Integer status){
        return chargeStandardMapper.updateStatus(id, status) == 1;
    }

    @Override
    public boolean batchDelChargeStandard(String ids){
        String[] arr = ids.split(",");
        List<Long> delList = new ArrayList<>();
        List<String> cannotDelList = new ArrayList<>();
        for(String id : arr) {
            long roleId = Long.valueOf(id);
            delList.add(Long.valueOf(id));
        }
        int row = chargeStandardMapper.batchDelete(delList);
        return row > 0;
    }




    @Override
    public PageInfo getChargeMethodList(int pageNum, int pageSize, ChargeMethod chargeMethod){
        PageHelper.startPage(pageNum, pageSize);
        List<ChargeMethod> list = chargeMethodMapper.getList(BeanMapUtils.toMap(chargeMethod));
        return new PageInfo(list);
    }
    @Override
    public boolean addChargeMethod(ChargeMethod chargeMethod){
        return chargeMethodMapper.insertSelective(chargeMethod)==1;
    }
    @Override
    public boolean deleteChargeMethod(Long chargeMethodId){
        return chargeMethodMapper.deleteByPrimaryKey(chargeMethodId)==1;
    }
    @Override
    public boolean updateChargeMethod(ChargeMethod chargeMethod){
        return chargeMethodMapper.updateByPrimaryKeySelective(chargeMethod)==1;
    }
    @Override
    public boolean updateChargeMethodStatus(Long id, Integer status){
        return chargeMethodMapper.updateStatus(id, status) == 1;
    }

    @Override
    public boolean batchDelChargeMethod(String ids){
        String[] arr = ids.split(",");
        List<Long> delList = new ArrayList<>();
        List<String> cannotDelList = new ArrayList<>();
        for(String id : arr) {
            long roleId = Long.valueOf(id);
            delList.add(Long.valueOf(id));
        }
        int row = chargeMethodMapper.batchDelete(delList);
        return row > 0;
    }

        //中控的计费标准console_recharge_standard表
    @Override
    public PageInfo getConsoleChargeStandardList(int pageNum, int pageSize, ConsoleChargeStandard consoleChargeStandard){
        PageHelper.startPage(pageNum, pageSize);
        List<ConsoleChargeStandard> list = consoleChargeStandardMapper.getConsoleChargeStandardList(BeanMapUtils.toMap(consoleChargeStandard));
        return new PageInfo(list);
    }

    @Override
    public boolean addConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard){
        return consoleChargeStandardMapper.insertSelective(consoleChargeStandard)==1;
    }
    @Override
    public boolean updateConsoleChargeStandard(ConsoleChargeStandard consoleChargeStandard){
        return consoleChargeStandardMapper.updateByPrimaryKeySelective(consoleChargeStandard)==1;
    }

    @Override
    public boolean batchDelConsoleChargeStandard(String ids){
        String[] arr = ids.split(",");
        List<Long> delList = new ArrayList<>();
        List<String> cannotDelList = new ArrayList<>();
        for(String id : arr) {
            long roleId = Long.valueOf(id);
            delList.add(Long.valueOf(id));
        }
        int row = consoleChargeStandardMapper.batchDelete(delList);
        return row > 0;
    }

    @Override
    public boolean updateConsoleChargeStandardStatus(Long id, Integer status){
        return consoleChargeStandardMapper.updateStatus(id, status) == 1;
    }

}
