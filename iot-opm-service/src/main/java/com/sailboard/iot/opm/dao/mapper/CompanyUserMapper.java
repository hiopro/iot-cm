package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.CompanyUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface CompanyUserMapper {
    @Delete({
        "delete from company_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into company_user (id, user_id, ",
        "company_id)",
        "values (#{id,jdbcType=BIGINT}, #{userId,jdbcType=BIGINT}, ",
        "#{companyId,jdbcType=BIGINT})"
    })
    int insert(CompanyUser record);

    int insertSelective(CompanyUser record);

    @Select({
        "select",
        "id, user_id, company_id",
        "from company_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    CompanyUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CompanyUser record);

    @Update({
        "update company_user",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "company_id = #{companyId,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(CompanyUser record);
}