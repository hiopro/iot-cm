package com.sailboard.iot.opm.service.company;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Agent;

/**
 * @author: liyl
 * @date: 2017/11/5 下午4:33
 * @since 1.0.0
 */
public interface AgentService {

    PageInfo getList(int pageNum, int pageSize, Agent agent);

}
