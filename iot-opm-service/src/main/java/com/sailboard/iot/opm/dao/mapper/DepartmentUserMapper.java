package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.DepartmentUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface DepartmentUserMapper {
    @Delete({
        "delete from department_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Delete({
            "delete from department_user",
            "where user_id = #{userId,jdbcType=BIGINT}"
    })
    int deleteByUserId(Long userId);

    @Insert({
        "insert into department_user (id, user_id, ",
        "department_id)",
        "values (#{id,jdbcType=BIGINT}, #{userId,jdbcType=BIGINT}, ",
        "#{departmentId,jdbcType=BIGINT})"
    })
    int insert(DepartmentUser record);

    int insertSelective(DepartmentUser record);

    @Select({
        "select",
        "id, user_id, department_id",
        "from department_user",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    DepartmentUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DepartmentUser record);

    @Update({
        "update department_user",
        "set user_id = #{userId,jdbcType=BIGINT},",
          "department_id = #{departmentId,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(DepartmentUser record);

    List<DepartmentUser> getDepartmentUserList(Map map);

    int batchDelete(List<Long> ids);

    int batchInsert(List<DepartmentUser> list);
}