package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ConsumeRecord;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ConsumeRecordMapper {
    @Delete({
        "delete from consume_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into consume_record (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "amount, account_id, ",
        "consume_method, charge_record_id, ",
        "user_id, user_name, ",
        "remarks，type,account_type)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{amount,jdbcType=DECIMAL}, #{accountId,jdbcType=BIGINT}, ",
        "#{consumeMethod,jdbcType=TINYINT}, #{chargeRecordId,jdbcType=BIGINT}, ",
        "#{userId,jdbcType=BIGINT}, #{userName,jdbcType=VARCHAR}, ",
        "#{remarks,jdbcType=VARCHAR}, #{type,jdbcType=TINYINT},#{account_type,jdbcType=VARCHAR})"
    })
    int insert(ConsumeRecord record);

    int insertSelective(ConsumeRecord record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "amount, account_id, consume_method, charge_record_id, user_id, user_name, remarks, type,account_type",
        "from consume_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ConsumeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ConsumeRecord record);

    @Update({
        "update consume_record",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "amount = #{amount,jdbcType=DECIMAL},",
          "account_id = #{accountId,jdbcType=BIGINT},",
          "consume_method = #{consumeMethod,jdbcType=TINYINT},",
          "charge_record_id = #{chargeRecordId,jdbcType=BIGINT},",
          "user_id = #{userId,jdbcType=BIGINT},",
          "user_name = #{userName,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR}",
            "type = #{type,jdbcType=TINYINT}",
            "account_type = #{account_type,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ConsumeRecord record);
    //列出消费记录
    List<ConsumeRecord> listConsumeRecord(Map map);

    //更新记录状态
    @Update({
            "update consume_record",
            "set status = #{status,jdbcType=TINYINT},",
            "remarks = #{remarks,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateRecordtStatus(@Param("id") Long accountID, @Param("status") Integer status,@Param("remarks")String remarks);

    //列出提现申请记录
    List<ConsumeRecord> getWithdrawalApplication(Map map);
}