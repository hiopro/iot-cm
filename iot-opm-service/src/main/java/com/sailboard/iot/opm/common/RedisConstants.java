package com.sailboard.iot.opm.common;

/**
 * Created by tiansj on 2017/8/18.
 */
public final class RedisConstants {

    public static final String TOKEN = "token_";
    public static final String MOBILE = "mobile_";
    public static final String IP_MAX_SMS = "ip_max_sms_";
    public static final String USER_FANS_COUNT = "user_fans_count_";
    public static final String RANDOM_USER = "random_user_";

}
