package com.sailboard.iot.opm.dao.entity;

import java.io.Serializable;

/**
 * @author: liyl
 * @date: 2017/11/5 上午10:44
 * @since 1.0.0
 *
 * 代理商实体
 */
public class Agent implements Serializable {

    private Long id;

    private String agentName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
}
