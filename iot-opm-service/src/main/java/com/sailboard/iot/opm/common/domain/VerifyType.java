package com.sailboard.iot.opm.common.domain;

/**
 * Created by tiansj on 15/3/31.
 */
public enum VerifyType {
    IMAGE,
    NUMBER,
    EMAIL_LINK
}
