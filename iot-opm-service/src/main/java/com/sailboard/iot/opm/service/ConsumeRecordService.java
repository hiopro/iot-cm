package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.ConsumeRecord;

public interface ConsumeRecordService {
    PageInfo getConsume_RecordList(int pageNum, int pageSize,ConsumeRecord consumeRecord);
    boolean updateConsumeRecordSelective(ConsumeRecord consumeRecord);
    boolean updateStatus(Long id,Integer status,String remarks);
    PageInfo getWithdrawalApplication(int pageNum, int pageSize,ConsumeRecord consumeRecord);
    boolean addConsumeRecordWithdrawal(ConsumeRecord consumeRecord);
}
