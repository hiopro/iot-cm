package com.sailboard.iot.opm.service;

import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.dao.entity.DepartmentUser;
import com.sailboard.iot.opm.dao.entity.User;

public interface DepartmentUserService {
    PageInfo getDepartmentUserList(int pageNum, int pageSize, DepartmentUser departmentUser);
    boolean batchDelete(String ids);
    boolean add(DepartmentUser departmentUser);
    boolean edit(DepartmentUser departmentUser);
    JsonResult bindDepartment(Long userId, String departmentIds);
}
