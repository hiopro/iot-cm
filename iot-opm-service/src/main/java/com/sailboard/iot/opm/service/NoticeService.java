package com.sailboard.iot.opm.service;


import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.common.JsonResult;
import com.sailboard.iot.opm.dao.entity.Notice;

/**
 * Created by tiansj on 15/5/28.
 */
public interface NoticeService {

    public JsonResult selectById(Long noticeId);

    public JsonResult insert(Notice record);

    public JsonResult updateById(Notice record);

    public JsonResult deleteById(Long noticeId);

    PageInfo getNoticeList(int pageNum, int pageSize, Notice param);

    public boolean batchDelete(String ids);
}
