package com.sailboard.iot.opm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sailboard.iot.opm.dao.entity.Department;
import com.sailboard.iot.opm.dao.mapper.DepartmentMapper;
import com.sailboard.iot.opm.service.DepartmentService;
import com.sailboard.iot.opm.utils.BeanMapUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class DepartmentServiceImpl implements DepartmentService{
    @Resource
    DepartmentMapper departmentMapper;
    public PageInfo getDepartmentList(int pageNum, int pageSize, Department department) {
        PageHelper.startPage(pageNum, pageSize);
        List<Department> list = departmentMapper.getDepartmentList(BeanMapUtils.toMap(department));
        return new PageInfo(list);
    }

    public boolean addDepartment(Department department){
        return departmentMapper.insertSelective(department) == 1;
    }

    public boolean updateDepartment(Department department){
        return departmentMapper.updateByPrimaryKeySelective(department) == 1;
    }

    public boolean deleteDepartment(Long departmentID ){
        return departmentMapper.deleteByPrimaryKey(departmentID) == 1;
    }

    public boolean batchDeleteDepartment(String ids){
        List<Long> list = new ArrayList<Long>();
        String[] arr = ids.split(",");
        for(String id : arr){
            Long departmentID = Long.valueOf(id);
            if(departmentID > 10){
                list.add(departmentID);
            }
        }
        return departmentMapper.batchDeleteDepartment(list) > 0;
    }

    @Override
    public boolean updateDepartmentStatus(Long id, Integer status) {
        return departmentMapper.updateDepartmentStatus(id,status) == 1;
    }

    @Override
    public List<Department> getUserDepartment(Long UserId){
        return departmentMapper.getUserDepartment(UserId);
    }

    @Override
    public Department getDepartmentById(Long departmentId){
        return departmentMapper.selectByPrimaryKey(departmentId);
    }

    @Override
    public Department getDepartmentByName(String name,Long companyId){
        return departmentMapper.getDepartmentByName(name,companyId);
    }

    @Override
    public Department getOtherDepartmentByName(String name,Long id,Long companyId){
        return departmentMapper.getOtherDepartmentByName(name,id,companyId);
    }

}
