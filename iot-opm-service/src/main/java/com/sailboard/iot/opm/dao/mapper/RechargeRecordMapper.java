package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.RechargeRecord;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

public interface RechargeRecordMapper {
    @Delete({
        "delete from recharge_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into recharge_record (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "amount, account_id, ",
        "recharge_method, recharge_card_id, ",
        "user_id, user_name, ",
        "remarks)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{amount,jdbcType=DECIMAL}, #{accountId,jdbcType=BIGINT}, ",
        "#{rechargeMethod,jdbcType=TINYINT}, #{rechargeCardId,jdbcType=BIGINT}, ",
        "#{userId,jdbcType=BIGINT}, #{userName,jdbcType=VARCHAR}, ",
        "#{remarks,jdbcType=VARCHAR}, #{type,jdbcType=TINYINT},#{account_type,jdbcType=VARCHAR})"
    })
    int insert(RechargeRecord record);

    int insertSelective(RechargeRecord record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "amount, account_id, recharge_method, recharge_card_id, user_id, user_name, remarks,type,account_type",
        "from recharge_record",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    RechargeRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RechargeRecord record);

    @Update({
        "update recharge_record",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "amount = #{amount,jdbcType=DECIMAL},",
          "account_id = #{accountId,jdbcType=BIGINT},",
          "recharge_method = #{rechargeMethod,jdbcType=TINYINT},",
          "recharge_card_id = #{rechargeCardId,jdbcType=BIGINT},",
          "user_id = #{userId,jdbcType=BIGINT},",
          "user_name = #{userName,jdbcType=VARCHAR},",
          "remarks = #{remarks,jdbcType=VARCHAR}",
            "type = #{type,jdbcType=TINYINT}",
            "account_type = #{account_type,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(RechargeRecord record);

    //列出充值记录
    List<RechargeRecord> listRechargeRecord(Map map);
}