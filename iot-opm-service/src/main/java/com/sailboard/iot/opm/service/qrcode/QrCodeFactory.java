/*
 * Copyright (C) 2018 Baidu, Inc. All Rights Reserved.
 */
package com.sailboard.iot.opm.service.qrcode;

import java.util.HashMap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.stereotype.Component;

/**
 * @author: liyl
 * @date: 2017/11/26 下午8:26
 * @since 1.0.0
 */
@Component
public class QrCodeFactory {

    public  BitMatrix create(String content) {
        int width = 300;
        int height = 300;
        String format = "png";
        //定义二维码的参数
        HashMap map = new HashMap();
        //设置编码
        map.put(EncodeHintType.CHARACTER_SET, "utf-8");
        //设置纠错等级
        map.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        map.put(EncodeHintType.MARGIN, 2);

        try {
            //生成二维码
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height);
            return bitMatrix;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }
}
