package com.sailboard.iot.opm.dao.mapper;

import com.sailboard.iot.opm.dao.entity.ChargeStandard;
import com.sailboard.iot.opm.dao.entity.RechargeCard;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ChargeStandardMapper {
    @Delete({
        "delete from charge_standard",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into charge_standard (id, name, ",
        "status, sequence_number, ",
        "create_time, update_time, ",
        "creater, creater_id, ",
        "power_lower, power_upper, ",
        "unit_rice, unit_type, ",
        "terminal_id)",
        "values (#{id,jdbcType=BIGINT}, #{name,jdbcType=VARCHAR}, ",
        "#{status,jdbcType=TINYINT}, #{sequenceNumber,jdbcType=BIGINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{creater,jdbcType=VARCHAR}, #{createrId,jdbcType=BIGINT}, ",
        "#{powerLower,jdbcType=BIGINT}, #{powerUpper,jdbcType=BIGINT}, ",
        "#{unitRice,jdbcType=DECIMAL}, #{unitType,jdbcType=TINYINT}, ",
        "#{terminalId,jdbcType=BIGINT})"
    })
    int insert(ChargeStandard record);

    int insertSelective(ChargeStandard record);

    @Select({
        "select",
        "id, name, status, sequence_number, create_time, update_time, creater, creater_id, ",
        "power_lower, power_upper, unit_rice, unit_type, terminal_id",
        "from charge_standard",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("BaseResultMap")
    ChargeStandard selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChargeStandard record);

    @Update({
        "update charge_standard",
        "set name = #{name,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=TINYINT},",
          "sequence_number = #{sequenceNumber,jdbcType=BIGINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "creater = #{creater,jdbcType=VARCHAR},",
          "creater_id = #{createrId,jdbcType=BIGINT},",
          "power_lower = #{powerLower,jdbcType=BIGINT},",
          "power_upper = #{powerUpper,jdbcType=BIGINT},",
          "unit_rice = #{unitRice,jdbcType=DECIMAL},",
          "unit_type = #{unitType,jdbcType=TINYINT},",
          "terminal_id = #{terminalId,jdbcType=BIGINT}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(ChargeStandard record);

    /*自定义*/
    List<ChargeStandard> getList(Map map);

    @Update({
            "update charge_standard",
            "set status = #{status,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);

    int batchDelete(List<Long> ids);
}