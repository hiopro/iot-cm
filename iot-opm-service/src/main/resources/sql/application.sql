-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 127.0.0.1    Database: back_universal
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice` (
  `notice_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` text,
  `source` varchar(100) DEFAULT NULL,
  `is_use_link` tinyint(2) DEFAULT '1' COMMENT '是否使用外部链接：1-不使用，2-使用',
  `link` varchar(200) DEFAULT NULL,
  `publish_time` datetime DEFAULT NULL,
  `publish_status` tinyint(2) DEFAULT '1' COMMENT '发布状态：1-未发布，2-已发布',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `category` tinyint(2) DEFAULT NULL COMMENT '分类：1-网站公告，2-媒体公告',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--


--
-- Table structure for table `sys_resource`
--

DROP TABLE IF EXISTS `sys_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：1-可用，2不可用',
  `sort` bigint(20) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_resource`
--

LOCK TABLES `sys_resource` WRITE;
/*!40000 ALTER TABLE `sys_resource` DISABLE KEYS */;
INSERT INTO `sys_resource` VALUES (1,0,'系统设置','/','icon-1012333',1,1,'2015-06-21 13:07:40','2015-06-25 17:03:49'),(2,1,'权限设置','/','icon-wrench',1,2,'2015-06-25 14:20:09','2015-06-25 17:25:37'),(3,1,'基础设置','/','icon-wrench',1,3,'2015-06-25 14:14:41','2015-06-25 17:25:37'),(4,2,'用户管理','/view/privilege/user.html','icon-user',1,4,'2015-06-21 13:07:40','2015-06-25 17:25:37'),(5,2,'角色管理','/view/privilege/role.html','icon-vcard',1,5,'2015-06-21 13:07:40','2015-06-25 17:25:37'),(6,2,'菜单管理','/view/privilege/resource.html','icon-application_cascade',1,6,'2015-06-21 13:10:17','2015-06-25 17:25:37'),(7,3,'数据字典','/view/base/dictionary.html','icon-application_view_detail',1,7,'2015-06-25 14:18:30','2015-06-25 17:25:37'),(8,3,'日志管理','/view/base/log.html','icon-application_view_detail',1,8,'2015-06-25 14:18:30','2015-06-25 17:25:37'),(9,0,'财务管理','/',NULL,1,9,'2015-06-25 17:42:26','2015-06-25 17:42:26'),(10,9,'充值管理','/',NULL,1,10,'2015-06-25 17:43:28','2015-06-25 17:44:40'),(11,9,'提现管理','/',NULL,1,11,'2015-06-25 17:43:28','2015-06-25 17:44:40'),(12,10,'到账确认','/',NULL,1,12,'2015-06-25 17:44:40','2015-06-25 17:44:40'),(13,10,'到账审核','/',NULL,1,13,'2015-06-25 17:44:40','2015-06-25 17:44:40'),(14,11,'提现确认','/',NULL,1,14,'2015-06-25 17:45:20','2015-06-25 17:45:20'),(15,11,'提现审核','/',NULL,1,15,'2015-06-25 17:45:20','2015-06-25 17:45:20');
/*!40000 ALTER TABLE `sys_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '角色名称',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：1-可用，2-不可用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'超级管理员',1,'2015-06-21 12:45:25','2015-06-21 12:45:25');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_resource`
--

DROP TABLE IF EXISTS `sys_role_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_resource`
--

LOCK TABLES `sys_role_resource` WRITE;
/*!40000 ALTER TABLE `sys_role_resource` DISABLE KEYS */;
INSERT INTO `sys_role_resource` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,1,14),(15,1,15);
/*!40000 ALTER TABLE `sys_role_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(10) DEFAULT NULL COMMENT '加密盐',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '用户状态：1-启用，2-禁用',
  `real_name` varchar(30) DEFAULT NULL,
  `remark` varchar(60) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'superadmin','853e39d3507e952e0f2842ff7b7a4d5702e855bd7ad35e1d02e4be93c8f16ddd','d1ad8a36',1,'超级管理员','系统初始化管理帐号','superadmin@xuannniu.com','2015-06-21 12:44:31','2015-07-17 13:04:53');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1,1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-18 16:33:19


CREATE TABLE `sms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(20) NOT NULL COMMENT '验证码',
  `code` varchar(10) NOT NULL,
  `send_time` datetime NOT NULL COMMENT '发送时间',
  `expire_time` datetime NOT NULL COMMENT '过期时间',
  `type` tinyint(2) NOT NULL COMMENT '1短信，2提现，3系统重要提醒。',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息已发送记录表';




/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/12/11 0:23:32                           */
/*==============================================================*/



/*==============================================================*/
/* Table: company                                               */
/*==============================================================*/
/*create table company
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(50) not null,
   social_code          varchar(18) not NULL COMMENT '统一社会信用代码',
   business_license     varchar(20) default NULL comment '营业执照编码',
   status               tinyint(2) not null default 1 comment '用户状态：1-启用，2-禁用',
   address              varchar(100) comment '地址',
   leader               varchar(30) default NULL comment '负责人',
   mobile               bigint(11) default NULL comment '手机',
   email                varchar(45) default NULL,
   region               varchar(100) comment '经营区域',
   contract             bigint(20) comment '合同',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   sequence_number      bigint(20) comment '顺序号',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table company comment '公司表';*/

/*==============================================================*/
/* Table: company_role                                          */
/*==============================================================*/
create table company_role
(
   id                   bigint(20) not null auto_increment,
   company_id           bigint(20) not null,
   role_id              bigint(20) not null,
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

alter table company_role comment '公司角色表';

/*==============================================================*/
/* Table: company_user                                          */
/*==============================================================*/
create table company_user
(
   id                   bigint(20) not null auto_increment,
   user_id              bigint(20) not null,
   company_id           bigint(20) not null,
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

alter table company_user comment '公司人员表';

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '部门名称',
   company_id           varchar(64) not null comment '公司ID',
   status               tinyint(2) not null default 1 comment '部门状态：1-启用，2-禁用',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table department comment '部门表';

/*==============================================================*/
/* Table: department_user                                       */
/*==============================================================*/
create table department_user
(
   id                   bigint(20) not null auto_increment,
   user_id              bigint(20) not null,
   department_id        bigint(20) not null,
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

alter table department_user comment '部门人员表';



/*==============================================================*/
/* Table: company  修改公司 增加相关字段                                             */
/*==============================================================*/
drop table if exists company;


create table company
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(50) not null comment '公司名称',
   social_code          varchar(18) not null comment '统一社会信用代码',
   business_license     varchar(20) default NULL comment '营业执照编码',
   license_path         varchar(100) comment '营业执照附件路径',
   status               tinyint(2) not null default 1 comment '状态：1-启用，2-禁用',
   address              varchar(100) comment '地址',
   leader               bigint(20) default NULL comment '负责人（userID）',
   leader_name          varchar(50) comment '负责人名称',
   mobile               bigint(11) default NULL comment '手机',
   email                varchar(45) default NULL comment '邮箱',
   region               varchar(100) comment '经营区域',
   contract             bigint(20) comment '合同',
   contract_path        varchar(100) comment '合同附件路径',
   type                 int comment '公司类型0-平台公司，1-代理商，2-合作伙伴',
   founder              bigint(20) comment '创建人（userID）',
   founder_name         varchar(50) comment '创建人名称',
   remarks              varchar(200) comment '备注',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table company comment '公司表';

/*==============================================================*/
/* Table: sys_user  用户表增加公司及公司名称字段                */
/*==============================================================*/
alter table sys_user add company_id bigint(20) DEFAULT NULL;
alter table sys_user add company_name varchar(50) DEFAULT NULL;



/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/12/24 17:02:43                          */
/*==============================================================*/


drop table if exists account;

drop table if exists charge_method;

drop table if exists charge_record;

drop table if exists charge_standard;

drop table if exists charging_station;

drop table if exists console;

drop table if exists consume_record;

drop table if exists id_card;

drop table if exists recharge_card;

drop table if exists recharge_record;

drop table if exists terminal;

drop table if exists user_station;

/*==============================================================*/
/* Table: account                                               */
/*==============================================================*/
create table account
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   type                 tinyint(2) comment '账户类型',
   balance              decimal(18,4) comment '账户余额',
   user_id              bigint(20) comment '用户id',
   user_name            varchar(50) comment '用户名称',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table account comment '账户表[account]';

/*==============================================================*/
/* Table: charge_method                                         */
/*==============================================================*/
create table charge_method
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   charge_time          decimal(4,2) comment '充电时长',
   console_id           bigint(20) comment '中控箱ID',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table charge_method comment '充电方法表[charge_method]';

/*==============================================================*/
/* Table: charge_record                                         */
/*==============================================================*/
create table charge_record
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   start_time           datetime comment '开始时间',
   end_time             datetime comment '结束时间',
   terminal_id          bigint(20) comment '充电终端ID',
   charge_time          decimal(4,2) comment '充电时长',
   costs                decimal(4,2) comment '充电费用',
   station_name         varchar(50) comment '电站名称',
   terminal_number      varchar(50) comment '充电终端序列号',
   current_power        decimal(6,2) comment '当前功率',
   average_power        decimal(6,2) comment '平均功率',
   remarks              varchar(500) comment '备注',
   charger_id           bigint(20) comment '充电人',
   charger_name         varchar(50) comment '充电人名称',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table charge_record comment '充电记录表[charge_record]';

/*==============================================================*/
/* Table: charge_standard                                       */
/*==============================================================*/
create table charge_standard
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   power_lower          tinyint(5) comment '功率下限（W）',
   power_upper          tinyint(5) comment '功率上限（W）',
   unit_rice            decimal(4,2) comment '单价',
   unit_type            tinyint(2) comment '单位(1-元/小时，2-元/KWH)',
   terminal_id          bigint(20) comment '充电终端ID',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table charge_standard comment '计费标准表[charge_standard]';

/*==============================================================*/
/* Table: charging_station                                      */
/*==============================================================*/
create table charging_station
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   manager_id           bigint(20) comment '代理商ID',
   manager_name         varchar(50) comment '代理商名称',
   partener_id          bigint(20) comment '合作伙伴ID',
   partener_name        varchar(50) comment '合作伙伴名称',
   partener_mobile      varchar(20) comment '合作伙伴电话',
   operation_starttime  tinyint(2) comment '运营开始时间',
   operation_endtime    tinyint(2) comment '运营结束时间',
   address              varchar(500) comment '电站地址',
   longitude            decimal(18,14) comment '经度',
   latitude             decimal(18,14) comment '纬度',
   station_type         tinyint(2) comment '电站类型[1-快充，2-慢充]',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table charging_station comment '充电电站表[charging_station]';

/*==============================================================*/
/* Table: console                                               */
/*==============================================================*/
create table console
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   serial_number        varchar(50) comment '序列号',
   station_id           bigint(20) comment '电站id',
   terminal_number      tinyint(2) comment '终端数量',
   remarks              varchar(500) comment '备注',
   qr_code              varchar(500) comment '二维码',
   qr_code_url          varchar(500) comment '二维码对应请求地址',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table console comment '中空箱表[console]';

/*==============================================================*/
/* Table: consume_record                                        */
/*==============================================================*/
create table consume_record
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   amount               decimal(14,4) comment '消费金额',
   account_id           bigint(20) comment '账户id',
   consume_method       tinyint(2) comment '消费方式',
   charge_record_id     bigint(20) comment '充电记录id',
   user_id              bigint(20) comment '消费人id',
   user_name            varchar(50) comment '消费人名称',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table consume_record comment '消费记录表[consume_record]';

/*==============================================================*/
/* Table: id_card                                               */
/*==============================================================*/
create table id_card
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   serial_number        varchar(50) comment '卡片号',
   user_id              bigint(20) comment '用户id',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table id_card comment 'ID卡表[id_card]';

/*==============================================================*/
/* Table: recharge_card                                         */
/*==============================================================*/
create table recharge_card
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   station_id           bigint(20) comment '电站ID',
   face_value           tinyint(4) comment '面值',
   free_value           tinyint(2) comment '赠送额',
   expiration_date      date comment '失效日期',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table recharge_card comment '充值卡表[recharge_card]';

/*==============================================================*/
/* Table: recharge_record                                       */
/*==============================================================*/
create table recharge_record
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   amount               decimal(14,4) comment '充值金额',
   account_id           bigint(20) comment '账户id',
   recharge_method      tinyint(2) comment '充值方式',
   recharge_card_id     bigint(20) comment '充值卡ID',
   user_id              bigint(20) comment '充值人id',
   user_name            varchar(50) comment '充值人名称',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table recharge_record comment '充值记录表[recharge_record]';

/*==============================================================*/
/* Table: terminal                                              */
/*==============================================================*/
create table terminal
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   console_id           bigint(20) comment '中控箱ID',
   serial_number        varchar(50) comment '序列号',
   remarks              varchar(500) comment '备注',
   qr_code              varchar(500) comment '二维码',
   qr_code_url          varchar(500) comment '二维码对应请求地址',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table terminal comment '充电终端[terminal]';

/*==============================================================*/
/* Table: user_station                                          */
/*==============================================================*/
create table user_station
(
   id                   bigint(20) not null auto_increment,
   name                 varchar(30) not null comment '名称',
   status               tinyint(2) not null default 1 comment '状态',
   sequence_number      bigint(20) comment '顺序号',
   create_time          datetime not null default CURRENT_TIMESTAMP,
   update_time          datetime not null default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   creater              varchar(50) comment '创建人',
   creater_id           bigint(20) comment '创建人ID(sys_user.id)',
   user_id              bigint(20) comment '用户ID',
   user_name            varchar(50) comment '用户名称',
   station_id           bigint(20) comment '电站id',
   station_name         varchar(50) comment '电站名称',
   station_address      varchar(500) comment '电站地址',
   remarks              varchar(500) comment '备注',
   primary key (id)
)
ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

alter table user_station comment '常用电站表[user_station]';

