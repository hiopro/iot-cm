## 问题列表
 
 1.GIT使用规则（待确认）
 
 -
 
     快速简易使用规则
     1. 在IDEA中把写的新代码commit到本地仓库
     	a, git commit -am '注释'  //相当于git add . 和git commit 联合操作，会把所有变更的文件提交
     	b, 如果不想提交所有，通过idea自带的version control里local changes 挑选文件commit，界面化操作。
     	
     2. 在IDEA中把本地仓库代码push到远程gitee仓库
     	a, push之前先pull 是否有人提交，有冲突就先merge
     	b, git push origin head //提交到远程头分支。
     
     3. 从远程gitee仓库pull某几个文件到本地仓库
     	git pull
     
     4. 从本地仓库把刚pull到本地仓库的文件checkout到工程中
     	
     快速介绍
     1. clone
     2. checkout commit
     3. pull push
     4. fetch
     5. add
     
     
     暂时在master分支上开发
 
 2.文件结构目录用途
 
 -
 
     iot-opm-main 主要放静态页面和模版页面 
     	deploy 资源文件
     		bin 部署脚本
     		config 配置文件
     		static 静态文件
     		templates velocity模版文件
     	src
     		assembly 打包配置
     		main
     			java 
     				BmsApplication springboot启动类
     iot-opm-service service层到dao层的代码
     	src
     		main
     			java 业务代码
     				common 公用对象
     				dao 数据层
     					entity 数据对象
     					mapper 数据接口
     				framework 配置类
     				service 业务逻辑层
     					company 供应商业务
     		resources 资源文件
     			mapper mybaits的mapper.xml
     			sql 初始化sql
     			generatorConfig.xml mybaits.generator插件的配置，自动生成dao domain mapper
     			
     iot-opm-web controller层的代码 
     	src
     		main
     			java 
     				company 供应商接口 
     及其内部结构说明
 
 3.mybatis自动生成步骤流程介绍
 
 -
 
     1, 数据库建表
     2, 修改generatorConfig.xml
     	a, 修改 	<classPathEntry location="/Users/tiansj/.m2/repository/mysql/mysql-connector-java/5.1.22/mysql-connector-java-5.1.22.jar"/> jar包路径
     	b, 修改 <jdbcConnection driverClass="com.mysql.jdbc.Driver"
     			connectionURL="jdbc:mysql://127.0.0.1:3306/back_universal?characterEncoding=UTF-8"
     			userId="root" password="mrule">
     		</jdbcConnection> 
     	c, 修改 	    <javaModelGenerator targetPackage="com.sailboard.iot.opm.dao.entity"
     	    	targetProject="/Users/tiansj/workspace/jee-universal-bms/src/main/java" >
     	    </javaModelGenerator>
     		<sqlMapGenerator targetPackage="com.sailboard.iot.opm.dao.mapper"
     			targetProject="/Users/tiansj/workspace/jee-universal-bms/src/main/resources">
     		</sqlMapGenerator>
     		<javaClientGenerator targetPackage="com.sailboard.iot.opm.dao.mapper"
     			targetProject="/Users/tiansj/workspace/jee-universal-bms/src/main/java" type="MIXEDMAPPER">
     		</javaClientGenerator> 里面的targetProject路径
     	d,将要生存的表按照之前的结构复制一下
     		<table tableName="sys_user_role" domainObjectName="UserRole"
     			   enableCountByExample="false" enableUpdateByExample="false" enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false">
     		</table>
     	e,不需要生成的table注释掉
     	
     	d,cd到iot-opm-service下 执行mvn mybatis-generator:generate
     	
 
 4.一个简单增加数据功能流程：
 
 -
 
     1.html页面放哪
     	参照第二条 iot-opm-main的结构
     2.自动生成的mapper、po、xml会在哪
     	参照第三条generatorConfig.xml的配置
     3.需要编写几层文件，分别在哪
     	参照第二条
 
 5.多表数据对象VO如何使用
 
 -
 
     没有连表查询，单表对象联合组装成一个复合对象
 
 6.示例代码
 
 -
 
     1, 使用jdk1.8
     2, 刚开始不着急开发到dao层，因为都是单表查询，sql不会太多业务逻辑，当单表无法支持的时候记得及时把问题抛出来。
     3，AgentManagerController 简单的代理商查询页面 数据都是造的，并没有走到dao层
 
 	
 
 http://www.runoob.com/git/git-tutorial.html git教程
 
 https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/ git教程貌似打不开



